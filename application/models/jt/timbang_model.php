<?php
class Timbang_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
    }
	
	function view_data($num, $offset, $key, $category) {
	 	if($offset !='')
            $offset = $offset.',';
        
        $sql = "SELECT f.DocketID, f.DocketDate, a.AreaName, f.TruckID, c.ContractorName
				FROM ffbwbtrn f, areamst a, ffbwbdtltrn fd, contractormst c
				WHERE f.DocketID=fd.DocketID AND fd.AreaID=a.AreaID AND c.ContractorID=f.ContractorID";
		if($key!=='')
			$sql .=" AND $category LIKE '%$key%'";
		$sql .=" ORDER BY f.DocketID DESC, f.DocketDate DESC, fd.AreaName LIMIT $offset $num";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
	}
	
	function getNumRowsItem($key,$category) {
		$sql = "SELECT f.DocketID
				FROM ffbwbtrn f, areamst a, ffbwbdtltrn fd, contractormst c
				WHERE f.DocketID=fd.DocketID AND fd.AreaID=a.AreaID AND c.ContractorID=f.ContractorID";
		if($key !='')
			$sql .=" AND $category LIKE '%$key%'";
        $query = $this->db->query($sql);
        $num = $query->num_rows();
        return $num;
	}
		
	function getKendaraanFlg($nokendaraan, $notiket) {
		$sql	= "SELECT t.isInDB, t.isInside, t.isMatched, t.ProductID 
					FROM (
						SELECT 1 AS isInDB, 1 AS isInside, 1 AS isMatched, ProductID FROM dockettrn WHERE NoReg='$nokendaraan' AND DocketID='$notiket' AND IsInsideFlg=1
							UNION ALL
						SELECT 1 AS isInDB, 1 AS isInside, 0 AS isMatched, ProductID FROM dockettrn WHERE NoReg='$nokendaraan' AND IsInsideFlg=1
							UNION ALL
						SELECT 1 AS isInDB, 0 AS isInside, 0 AS isMatched, 0 AS ProductID FROM truckmst WHERE NoReg='$nokendaraan'
							UNION ALL
						SELECT 0 AS isInDB, 0 AS isInside, 0 AS isMatched, 0 AS ProductID
					) t LIMIT 1";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result[0];
	}
	
	function getDataKendaraan($nokendaraan) {
		$sql	= "SELECT  NoReg, TarraKG, DriverName, LicenceNo FROM truckmst WHERE NoReg='$nokendaraan'";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result[0];
	}
	
	function getProductList() {
		$sql	= "SELECT  ProductID, ProductName FROM productmst ORDER BY ID";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result;
	}
	
	function getAreaList() {
		$sql	= "SELECT  AreaID, AreaName FROM areamst ORDER BY IsDivision DESC, ID ASC";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result;
	}
	
	function getBlokList($areaid=null) {
		$sql	= "SELECT  ID, BlockID FROM areablockmst ";
		if( !is_null($areaid) ) {
			$sql .= "WHERE AreaID='$areaid' ";
		}
		$sql .= "ORDER BY BlockID";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result;
	}
	
	function getWBDate() {
		// $wbdate
		$sql	= "SELECT WBDate FROM configmst LIMIT 1";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result[0]['WBDate'];
	}
	
	function getNewDocketID() {
		// $wbdate
		$sql	= "SELECT LastDocketNo FROM configmst LIMIT 1";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		$lastno	= $result[0]['LastDocketNo'];
		$currentno	= $lastno + 1;
		return $currentno;
	}
	
	function getDataTiket($notiket) {
		$sql	= "SELECT  d.ID, d.DocketID, d.DocketDate, d.SPBNo, d.ProductID, d.NoReg, d.DriverName, d.LicenceNo, d.TripStart, d.TimeIn, d.WeightIn "
			. "FROM dockettrn d "
			. "WHERE d.DocketID='$notiket'";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result[0];
	}
	
	function getDataDetailArea($notiket) {
		$sql	= "SELECT  d.ID, d.DocketID, d.FFBWeight, d.AreaID, d.BlockID, d.TandanQty, a.AreaName "
			. "FROM ffbwbtrn d, areamst a "
			. "WHERE d.DocketID='$notiket' AND a.AreaID=d.AreaID ";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result;
	}
	
	function getDetailByIDDocket($id) {
		$sql	= "SELECT  f.ID, f.TandanQty "
			. "FROM ffbwbtrn f, dockettrn d "
			. "WHERE d.ID='$id' AND d.DocketID=f.DocketID ";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result;
	}
}