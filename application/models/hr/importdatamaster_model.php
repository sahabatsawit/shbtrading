<?php
class ImportDataMaster_Model extends CI_Model {
	
	function __construct(){
		parent::__construct();
		$this->_server = $this->load->database('server', TRUE);
		$this->_db	= $this->load->database('lokal', TRUE);
	}
	
	function getMasterFromServer($tablename){
		$fieldstringfromlokal	= $this->_getStringFieldsFromLokal($tablename);
		$this->_server->select($fieldstringfromlokal);
		$qry = $this->_server->get($tablename);
        $row = $qry->result_array();
        return $row;
	}
	
	function _getStringFieldsFromLokal($tablename) {
		$retval	= '';
		$fields = $this->_db->list_fields($tablename);
		$i	= 0;
		foreach ($fields as $field) {
			$retval	.=($i>0)?','.$field:$field;
			$i++;
		}
		return $retval;
	}

	function clearLokalMasterTable($tablename){		
		$sql = "TRUNCATE $tablename";
		$this->_db->query($sql);
	}

	function insertToLokal($tablename, $datatoinsert){
		
		$this->_db->insert($tablename, $datatoinsert);
	}	
	
	function restoreToLokal() {
		
		$querystringrestore	= file_get_contents(APPPATH.'/logs/mybackup.sql');
		$query = $this->db->query($querystringrestore);
		return $query;
	}
}
?>