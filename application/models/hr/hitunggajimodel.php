<?php
class hitunggajimodel extends CI_Model {
	
    function __construct() {
        parent::__construct();
    }

	function getCustoMstDataForPayroll(){
		//$sql = "SELECT PayrollProcessDate, WorkingDayPerWeek, MaxBiayaJabatan FROM customst";
		$sql = "SELECT PayrollProcessDate FROM customst";
        $query = $this->db->query($sql);
        $procedate = $query->result_array();
		return $procedate[0];
	}

	function getEmpMstList(){//----listing pegawai yg udah pernah gajian(ada id nya di payrolltrn)
		$sql = "SELECT e.EmpID, e.EmpName,
				e.EmpName AS NameID, #CONCAT(e.EmpID,' - ',e.EmpName) AS NameID,
				e.EmploymentID, e.NPWPNo, e.JamsostekFlg, p.PTKPDesc, e.WorkingDate
				FROM empmst e, ptkpmst p WHERE p.PTKPID=e.PTKPID
				AND e.EmpID IN (SELECT DISTINCT(EmpID) FROM payrolltrn WHERE TransType='G') ORDER BY e.EmpName";
        $query = $this->db->query($sql);
        return $query->result_array();
	}

	function getEmpMstRapelList($fiscyear, $prdmonth){//----listing pegawai yg bulan terakhir ada gaji nya(ada id nya di payrolltrn)
		$sql = "SELECT p.EmpID, e.BPJSFlg, e.JamsostekFlg, e.WorkingDate FROM payrolltrn p, empmst e
				WHERE e.EmpID=p.EmpID AND TransType='G'
					AND FiscYear=(SELECT YEAR(EndProcessDate) FROM rapelperiodetrn r WHERE r.FiscYear=$fiscyear AND r.PrdMth=$prdmonth)
					AND PrdMth=(SELECT MONTH(EndProcessDate) FROM rapelperiodetrn  r WHERE r.FiscYear=$fiscyear AND r.PrdMth=$prdmonth)
		AND e.ActiveFlg='1'";
        $query = $this->db->query($sql);
        return $query->result_array();
	}

	function getAttendedEmp($tglproses){
		list($fiscyear, $prdmonth, $datetrn) = explode("-", $tglproses);
		$prdmonth = $prdmonth + 0; //biar angka 0 didepan nya ilang
		
		$sql = "SELECT e.EmpID, e.EmploymentID, e.NPWPNo, e.JamsostekFlg, e.BPJSFlg, p.PTKPDesc, e.MaritalStatusID, e.PTKPID, e.WorkingDate, DATE_FORMAT(e.ResignDate,'%Y-%c') AS ResignDate,e.ResignDate AS TglResign
				FROM empmst e, ptkpmst p, attrn a, atdtltrn ad
				WHERE e.EmpID=ad.EmpID AND a.ID=ad.ID AND p.PTKPID=e.MaritalStatusID 
				AND a.StatusFlg='1' AND a.FiscYear='$fiscyear' AND a.PrdMth='$prdmonth' GROUP BY e.EmpID";
        $query = $this->db->query($sql);
        return $query->result_array();
	}

	function getPTKPAmt($empid){
		$sql = "SELECT p.PTKPAmt FROM empmst e, ptkpmst p WHERE p.PTKPID=e.PTKPID AND e.EmpID='$empid'";
        $query = $this->db->query($sql);
        $a= $query->result_array();
		return $a[0]['PTKPAmt'];
	}

	function getJPtgFromCustomst(){
		$sql = "SELECT JKKPtg, JKMPtg, JHTCompPtg, JHTEmpPtg, JPKKEmpPtg, JPKTKEmpPtg, UMRRateAmt FROM customst";
        $query = $this->db->query($sql);
        $result = $query->result_array();
		return $result[0];
	}

	function getGrossAmt($empid, $tglproses, $isKHL,  $employmentid, $workingdate='0000-00-00', $isGajiRapel=FALSE, $isnewemployee=FALSE){
		list($fiscyear, $prdmonth, $datetrn) = explode("-", $tglproses);
		$prdmonth = $prdmonth + 0; //biar angka 0 didepan nya ilang
		if($isGajiRapel){ // ini klo dipakai untuk ngitung gaji rapelan, karena waktu kerja(NmkDay) dan rate gaji nya pake month year yg beda.
			list($fiscyear, $prdmonth, $datetrn) = explode("-", $workingdate);
			$prdmonth = $prdmonth + 0;
		}
		$result[0]['GajiPokok']=0;
		$sql = "";
		if($isKHL){
			//$sql = "SELECT 
			//			(SELECT (e.GrossAmt) FROM empsalarymst e WHERE e.EmpID='$empid' AND  e.StartDate <= '$tglproses' ORDER BY e.StartDate DESC LIMIT 1) * SUM(ad.NmkDay)  AS GajiPokok,
			//			(SELECT (e.AlwAmt) FROM empsalarymst e WHERE e.EmpID='$empid' AND  e.StartDate <= '$tglproses' ORDER BY e.StartDate DESC LIMIT 1) * SUM(ad.NmkDay)  AS TunjTetap
			//		FROM attrn a, atdtltrn ad
			//		WHERE a.ID=ad.ID AND a.PrdMth='$prdmonth' AND a.FiscYear='$fiscyear' AND ad.EmpID='$empid' AND a.StatusFlg='1'";
			
			$sql = "SELECT 
						(SELECT (e.GrossAmt) FROM employmentsalarytrn e WHERE e.EmploymentID='$employmentid' AND  e.StartDate <= '$tglproses' ORDER BY e.StartDate DESC LIMIT 1) * SUM(ad.NmkDay)  AS GajiPokok,
						0  AS TunjTetap
					FROM attrn a, atdtltrn ad
					WHERE a.ID=ad.ID AND a.PrdMth='$prdmonth' AND a.FiscYear='$fiscyear' AND ad.EmpID='$empid' AND a.StatusFlg='1'";
		}
		else{
			//$sql = "SELECT GrossAmt AS GajiPokok, AlwAmt AS TunjTetap FROM empsalarymst
			//		WHERE EmpID='$empid' AND StartDate <= '$tglproses'
			//		ORDER BY StartDate DESC LIMIT 1"; // gaji terakhir yg berlaku pada bulan dan tahun tertentu untuk satu karyawan tertentu (nonKHL)
			
			
			
			$sql = "SELECT GrossAmt AS GajiPokok, 0 AS TunjTetap FROM employmentsalarytrn
					WHERE EmploymentID='$employmentid' AND StartDate <= '$tglproses'
					ORDER BY StartDate DESC LIMIT 1"; // gaji terakhir yg berlaku pada bulan dan tahun tertentu untuk satu karyawan tertentu (nonKHL)	
		}
        $query = $this->db->query($sql);
		if($query->num_rows()>0){
			$result = $query->result_array();
		}
		//-------------------------------------------
		if(!$isKHL) {	
			if($isnewemployee ){
				$dataworkingdays= $this->getWorkingDays($empid, $workingdate);
				$paidday		= $dataworkingdays['PaidDay'];
				$grossAmt	= $result[0]['GajiPokok'];
				$grossAmt = $grossAmt / 30 * $paidday;
				$result[0]['GajiPokok'] = $grossAmt;
			}
		}
		//----------------------------------------------
		return $result[0];
	}

	function getGajiSebulanToMth($empid, $prdmonth, $fiscyear){
		$sql = "SELECT
					SUM(RapelAmt) AS RapelAmt,
					SUM(GrossAmt) AS GrossAmt,
					SUM(JKKAmt) AS JKKAmt,
					SUM(JKMAmt) AS JKMAmt,
					SUM(JPKKEmpAmt) AS JPKKEmpAmt,
					SUM(JPKKCompAmt) AS JPKKCompAmt,
					SUM(JHTEmpAmt) AS JHTEmpAmt,
					SUM(DriverBonusAmt) AS DriverBonusAmt,
					SUM(OTAmt) AS OTAmt,
					SUM(RiceAlwAmt) AS RiceAlwAmt,
					SUM(AlwAmt) AS AlwAmt,
					SUM(UnpaidAmt) AS UnpaidAmt,
					SUM(TaxSupportAmt) AS TaxSupportAmt,
					SUM(PPHAmt) AS PPHAmt
				FROM payrolltrn WHERE PrdMth < $prdmonth AND FiscYear='$fiscyear' AND EmpID='$empid' AND TransType='G' GROUP BY EmpID";
		$query = $this->db->query($sql);
        $result = $query->result_array();
		$gajimthbefore = 0;
		if($query->num_rows()>0){
			$gajimthbefore = $result[0];
		}
		return $gajimthbefore;
	}

	function getTHRToMth($empid, $prdmonth, $fiscyear){
		$sql = "SELECT	SUM(GrossAmt) AS thr
				FROM payrolltrn WHERE PrdMth <= $prdmonth AND FiscYear='$fiscyear' AND EmpID='$empid' AND TransType='T' GROUP BY EmpID";
		$query = $this->db->query($sql);
        $result = $query->result_array();
		$thr = 0;
		if($query->num_rows()>0){
			$thr = $result[0]['thr'];
		}
		return $thr;
	}

	function getOTHrs($empid, $tglproses){
		list($fiscyear, $prdmonth, $datetrn) = explode("-", $tglproses);
		$prdmonth = $prdmonth + 0; //biar angka 0 didepan nya ilang
		$sql = "SELECT SUM(od.Mandays) AS OTMdy, (SUM(od.OT100Hrs * 1) + SUM(od.OT150Hrs * 1.5) + SUM(od.OT200Hrs * 2) +SUM(od.OT300Hrs * 3) +SUM(od.OT400Hrs * 4)) AS OTQty FROM ottrn o, otdtlemptrn od
				WHERE o.ID=od.ID AND o.StatusFlg='1' AND od.EmpID='$empid' AND MONTH(o.OTDate)='$prdmonth' AND YEAR(o.OTDate)='$fiscyear' GROUP BY od.EmpID";
        $query = $this->db->query($sql);
        $result = $query->result_array();
		$othrs = 0;
		if($query->num_rows()>0){
			$othrs = $result[0];
		}
		return $othrs;
	}

	function getAlwAndDedForEmp($empid, $tglproses){
		list($fiscyear, $prdmonth, $datetrn) = explode("-", $tglproses);
		$prdmonth = $prdmonth + 0; //biar angka 0 didepan nya ilang
		$sql = "SELECT SUM(adt.AllDedAmt) AS AlwDedAmt, a.AllDedType FROM alldedmst a, alldedtrn at, alldeddtltrn adt
				WHERE adt.EmpID='$empid' AND at.FiscYear='$fiscyear' AND at.PrdMth='$prdmonth' AND at.StatusFlg='1'
				AND at.AllDedTransID=adt.AllDedTransID AND a.AllDedID=adt.AllDedID GROUP BY a.AllDedType";
        $query = $this->db->query($sql);
        $resultquery = $query->result_array();

		$result['DedAmt'] = 0;
		$result['AlwAmt'] = 0;
		if($query->num_rows()==1 ){
			if($resultquery[0]['AllDedType']=='T'){
				$result['AlwAmt'] = $resultquery[0]['AlwDedAmt'];//potongan
			}
			else $result['DedAmt'] = $resultquery[0]['AlwDedAmt'];//potongan
		}
		else if($query->num_rows()==2){
			$result['DedAmt'] = $resultquery[0]['AlwDedAmt'];//potongan
			$result['AlwAmt'] = $resultquery[1]['AlwDedAmt'];//tunjangan
		}
		return $result;
	}

	function getLabContractData($empid, $tglproses){
		$labcontract['LabContractDay'] = 0;
		$labcontract['LabContractAmt'] = 0;
	    return $labcontract;
	}

	function getOTAmtKHL($employmentid, $tglproses, $otmdy){
		//$sql = "SELECT e.GrossAmt * $otmdy AS OTAmt FROM empsalarymst e
		//		WHERE e.EmpID='$empid' AND  e.StartDate <= '$tglproses' ORDER BY e.StartDate DESC LIMIT 1";
				
		$sql = "SELECT e.GrossAmt * $otmdy AS OTAmt FROM employmentsalarytrn e
				WHERE e.EmploymentID='$employmentid' AND  e.StartDate <= '$tglproses' ORDER BY e.StartDate DESC LIMIT 1";
				
		$query = $this->db->query($sql);
        $result = $query->result_array();
		if($query->num_rows()>0 ) return $result[0]['OTAmt'];
		else  return 0;
	}

	function getPPHRangePtg(){
		$sql = "SELECT BLimitAmt, TLimitAmt,TaxPtg,(TLimitAmt - BLimitAmt) AS JumlLimit FROM pphrangemst ORDER BY BLimitAmt ASC";
		$query = $this->db->query($sql);
        return $query->result_array();
	}

	function getDriverBonusAmt($empid, $tglproses){
		list($fiscyear, $prdmonth, $datetrn) = explode("-", $tglproses);
		$prdmonth = $prdmonth + 0; //biar angka 0 didepan nya ilang
		$result[0]['DriverBonusAmt'] = 0;
		$sql = "SELECT 
				( (dt.PremiMati * dt.PremiMatiAmt) + (dt.NonInap1 * dt.NonInap1Amt) + (dt.NonInap2 * dt.NonInap2Amt) + (dt.Inap1 * dt.Inap1Amt) + (dt.Inap2 * dt.Inap2Amt) )AS DriverBonusAmt,
				dt.EmpID
				FROM driverbonusdtltrn dt, driverbonustrn d
				WHERE d.ID=dt.ID AND d.PrdMth='$prdmonth' AND d.FiscYear='$fiscyear' AND dt.EmpID='$empid' AND d.StatusFlg='1'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0 ) {
			$result = $query->result_array();
		}
		return $result[0]['DriverBonusAmt'];
	}

	function getDriverRitaseBonusAmt($empid, $tglproses){
		list($fiscyear, $prdmonth, $datetrn) = explode("-", $tglproses);
		$prdmonth = $prdmonth + 0; //biar angka 0 didepan nya ilang
		$result[0]['DriverRitaseBonusAmt'] = 0;
		$sql = "SELECT
				( (dt.RitaseSiangQty * dt.RitaseSiangAmt) + (dt.RitaseMalamQty * dt.RitaseMalamAmt) + (dt.JangkosQty * dt.JangkosAmt) 
					+ (dt.RitaseMingguQty * dt.RitaseMingguAmt) + (dt.FFBAWQty * dt.FFBAWAmt) + (dt.FFBCDQty * dt.FFBCDAmt) ) AS DriverRitaseBonusAmt,
				dt.EmpID
				FROM driverritasebonusdtltrn dt, driverritasebonustrn d
				WHERE d.ID=dt.ID AND d.PrdMth='$prdmonth' AND d.FiscYear='$fiscyear' AND dt.EmpID='$empid' AND d.StatusFlg='1'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0 ) {
			$result = $query->result_array();
		}
		return $result[0]['DriverRitaseBonusAmt'];
	}

	function getWorkingDays($empid, $tglproses){
		list($fiscyear, $prdmonth, $datetrn) = explode("-", $tglproses);
		$prdmonth = $prdmonth + 0; //biar angka 0 didepan nya ilang
		$result[0] = array('PaidDay'=>0, 'UnpaidDay'=>0, 'MkrDay'=>0);
		$sql = "SELECT (SUM(NmkDay)+SUM(LbkDay)+SUM(SktDay)+SUM(CubDay)+SUM(PmbDay)+SUM(LbbDay)) AS PaidDay,
				(SUM(CutDay)+SUM(PmtDay)+SUM(LbtDay)) AS UnpaidDay, SUM(MkrDay) AS MkrDay
				FROM attrn h, atdtltrn d
				WHERE h.ID = d.ID AND h.FiscYear='$fiscyear' AND h.PrdMth='$prdmonth' AND h.StatusFlg='1' AND d.EmpID='$empid'
				GROUP BY d.EmpID";
		$query = $this->db->query($sql);
		if($query->num_rows()>0 ) {
			$result = $query->result_array();
		}
		return $result[0];
	}

	function getRiceAlwAmt($empid, $riceprice, $emplyid, $workingstartdate='0000-00-00'){
		list($fiscyear, $prdmonth, $datetrn) = explode("-", $workingstartdate);
		$amt=0;
		$sql = "SELECT ($riceprice * r.RiceAlwQty) AS RiceAlwAmt, c.FixedEmployeeRiceAlwAmt, ep.RiceAlwTypeID
				FROM payrolltrn p, ricealwmst r, customst c, employmentmst ep
				WHERE p.EmpID='$empid' AND p.FiscYear='$fiscyear' AND p.PrdMth='$prdmonth' 
					AND p.TransType='G' AND p.MaritalStatusID=r.RiceAlwID AND ep.EmploymentID='$emplyid'";
		if($fiscyear=='0000') { //<---ini berarti untuk hitung gaji..bukan untuk hitung rapel
			$sql = "SELECT ($riceprice * r.RiceAlwQty) AS RiceAlwAmt, c.FixedEmployeeRiceAlwAmt, ep.RiceAlwTypeID
				FROM empmst e, ricealwmst r, customst c, employmentmst ep
				WHERE e.EmpID='$empid' AND e.MaritalStatusID=r.RiceAlwID AND ep.EmploymentID='$emplyid'";
		}
		$query = $this->db->query($sql);
		if($query->num_rows()>0 ) {
		    $retval = $query->result_array();
		    if( $retval[0]['RiceAlwTypeID']=='F'){
				$amt = $retval[0]['FixedEmployeeRiceAlwAmt'];
			}
			elseif($retval[0]['RiceAlwTypeID']=='S'){
				$amt = $retval[0]['RiceAlwAmt'];
			}
			else{//$retval[0]['RiceAlwTypeID']=='N' klo N amt nya harus 0
				$amt = 0;
			}
		}
		return $amt;
	}
	
	

	function getOTRate($emplyid, $tglproses){
		$row[0]['OTRateAmt']=0;
		$sql  = "SELECT OTRateAmt From employmentsalarytrn where EmploymentID='$emplyid' AND StartDate <= '$tglproses'
					ORDER BY StartDate DESC LIMIT 1";
		$qry    = $this->db->query($sql);
		if($qry->num_rows()>0 ) {
			$row    = $qry->result_array();
		}
		return $row[0]['OTRateAmt'];
	}

	function getEmpList($fiscyear, $prdmth, $checkroll){
		$sql  = "SELECT p.EmpID, e.EmpName 
				FROM payrolltrn p, empmst e, checkrollmst c WHERE p.EmpID=e.EmpID AND p.FiscYear='$fiscyear' AND PrdMth='$prdmth' AND c.CheckrollID=e.CheckrollID  AND p.TransType='G'";
		if($checkroll!='all')
			$sql .= " AND c.CheckrollID='$checkroll'";

		$sql .= " ORDER BY c.CheckrollID, e.EmpName ASC";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();

		return $row;
	}

	function getCheckrollMst(){
		$sql = "SELECT CheckrollID, CheckrollName FROM checkrollmst ORDER BY CheckrollName ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
	}
	
	function getEmploymentType($employmentid){
		$result[0]['EmploymentType']='x';
		$sql = "SELECT EmploymentType FROM employmentmst where EmploymentID='$employmentid' ";
        $query = $this->db->query($sql);
		if($query->num_rows()>0 ) {
			$result = $query->result_array();
		}
        return $result[0]['EmploymentType'];
	}

	function getGajiKaryawan($bulan, $tahun, $bagian, $karyawan){
		$sql = "SELECT e.EmpID, e.EmpName, p.GrossAmt, p.JHTEmpAmt, p.OTHrs, (p.OTAmt + p.DriverBonusAmt +  p.AlwAmt) AS LemburPremi, p.RapelAmt,
				p.DedAmt, p.PaidDay, p.UnpaidAmt, p.RiceAlwAmt, j.JobName, pt.PTKPDesc, em.EmploymentName,
				p.JPKKEmpAmt
				FROM payrolltrn p, empmst e, jobmst j, employmentmst em, ptkpmst pt
				WHERE p.EmpID=e.EmpID AND p.PrdMth='$bulan' AND p.FiscYear='$tahun' AND p.TransType='G' AND
				e.EmploymentID=em.EmploymentID AND e.PTKPID=pt.PTKPID AND e.JobID=j.JobID";
		
		if($bagian != 'all')
			$sql .= "  AND e.CheckrollID='$bagian'";
			
		if($karyawan != 'all')
			$sql .= "  AND e.EmpID='$karyawan'";
		
		$sql .= " ORDER BY e.EmpName ASC";
		
        $query = $this->db->query($sql);
        return $query->result_array();
	}

	function getEmpylIDEmpMst($empid){//$today is mktime return
		$row[0]['EmploymentID']	= '0';
		$sql	= "SELECT EmploymentID FROM empmst p
				  WHERE EmpID='$empid'";
		$qry	= $this->db->query($sql);
		if($qry->num_rows()>0 ) {
			$row    = $qry->result_array();
		}
		return $row[0]['EmploymentID'];
	}
	
	function getEmpylID($empid, $today){//$today is mktime return
		$row[0]['EmploymentID']=0;
		$fiscyear = date("Y",$today);
		$prdmth	= date("n",$today);
		$sql	= "SELECT p.EmploymentID FROM payrolltrn p
				  WHERE p.EmpID='$empid' AND p.FiscYear='$fiscyear' AND p.PrdMth='$prdmth' AND p.TransType='G'";
		$qry	= $this->db->query($sql);
		if($qry->num_rows()>0 ) {
			$row    = $qry->result_array();
		}
		return $row[0]['EmploymentID'];
	}

	function getPayrollTrn($empid, $workingstartdate,$isHitungRapel=TRUE){
		list($fiscyear, $prdmonth, $datetrn) = explode("-", $workingstartdate);
		$result[0]['PrdMth'] = $prdmonth;
		$result[0]['GrossAmt'] = 0;
		$result[0]['RapelAmt'] = 0;
		$result[0]['AlwAmt'] = 0;
		$result[0]['JKKAmt'] = 0;
		$result[0]['JKMAmt'] = 0;
		$result[0]['JPKKEmpAmt'] = 0;
		$result[0]['JPKKCompAmt'] = 0;
		$result[0]['OTAmt'] = 0;
		$result[0]['RiceAlwAmt'] = 0;
		$result[0]['UnpaidAmt'] = 0;
		$result[0]['DriverBonusAmt'] = 0;
		$result[0]['JHTCompAmt'] = 0;
		$result[0]['JHTEmpAmt'] = 0;

		if($isHitungRapel) {
			$result[0]['gajiditerima']	= 0;
			$result[0]['PTKPDesc']		= '';
		}
		$result[0]['OTHrs'] = 0;
		$result[0]['OTMdy'] = 0;
		$result[0]['OTHrs'] = 0;
		$result[0]['PaidDay'] = 0;
		$result[0]['MkrDay'] = 0;
		$result[0]['UnpaidDay'] = 0;
		$sql	="SELECT PrdMth, GrossAmt, RapelAmt, AlwAmt, JKKAmt, JKMAmt, JPKKEmpAmt, JPKKCompAmt, OTAmt, RiceAlwAmt, UnpaidAmt, DriverBonusAmt, JHTCompAmt, JHTEmpAmt,";
		if($isHitungRapel) {
			$sql .="(GrossAmt + OTAmt) AS gajiditerima, PTKPDesc,";
		}			
		$sql	.="OTHrs, OTMdy, OTHrs, PaidDay, MkrDay, UnpaidDay FROM payrolltrn p";
		if($isHitungRapel) {
			$sql .=", ptkpmst ptkp";
		}
		$sql	.=" WHERE p.EmpID='$empid' AND FiscYear='$fiscyear' AND PrdMth='$prdmonth' AND TransType='G'";
		if($isHitungRapel) {
			$sql .=" AND ptkp.PTKPID=p.MaritalStatusID";
		}
		$query	= $this->db->query($sql);
		if($query->num_rows()>0 ) {
			$result = $query->result_array();
		}
		return $result[0];
	}

	function getPayrollTrn_____($empid, $workingstartdate,$isHitungRapel=TRUE){
		list($fiscyear, $prdmonth, $datetrn) = explode("-", $workingstartdate);
		$result[0]['PrdMth'] = $prdmonth;
		$result[0]['GrossAmt'] = 0;
		$result[0]['RapelAmt'] = 0;
		$result[0]['AlwAmt'] = 0;
		$result[0]['JKKAmt'] = 0;
		$result[0]['JKMAmt'] = 0;
		$result[0]['JPKKEmpAmt'] = 0;
		$result[0]['OTAmt'] = 0;
		$result[0]['RiceAlwAmt'] = 0;
		$result[0]['UnpaidAmt'] = 0;
		$result[0]['DriverBonusAmt'] = 0;
		$result[0]['JHTEmpAmt'] = 0;
		$sql	="SELECT PrdMth, SUM(GrossAmt) AS GrossAmt, SUM(RapelAmt) AS RapelAmt,
					SUM(AlwAmt) AS AlwAmt, SUM(JKKAmt) AS JKKAmt, SUM(JKMAmt) AS JKMAmt, SUM(JPKKEmpAmt) AS JPKKEmpAmt,
					SUM(OTAmt) AS OTAmt, SUM(RiceAlwAmt) AS RiceAlwAmt, SUM(UnpaidAmt) AS UnpaidAmt, SUM(DriverBonusAmt) AS DriverBonusAmt,
					SUM(JHTEmpAmt) AS JHTEmpAmt FROM payrolltrn
				WHERE EmpID='$empid' AND FiscYear='$fiscyear' AND PrdMth='$prdmonth' GROUP BY PrdMth";
		$query	= $this->db->query($sql);
		if($query->num_rows()>0 ) {
			$result = $query->result_array();
		}
		return $result[0];
	}

	function getEmpDataByPayrolltrn($emp, $fiscyear, $prdmonth){
		$empsql = "SELECT e.EmpID, e.EmploymentID, e.NPWPNo, e.JamsostekFlg, p.PTKPDesc, e.WorkingDate, DATE_FORMAT(e.ResignDate,'%Y-%c') AS ResignDate
		FROM empmst e, ptkpmst p, payrolltrn pr
		WHERE p.PTKPID=e.MaritalStatusID AND pr.EmpID=e.EmpID AND pr.FiscYear='$fiscyear' AND pr.PrdMth='$prdmonth' AND TransType='G'";
		if($emp!='all'){
			$empsql .= " AND e.EmpID='$emp'";
		}
		$empsql .= " GROUP BY pr.EmpID";
		$query= $this->db->query($empsql);
		return $query->result_array();
	}

	function getDataRapelTrn($empid, $prdmonth, $fiscyear){
		$retval = array('RapelAmt'=>0,'GrossAmt'=>0,'OTAmt'=>0,'UnpaidAmt'=>0, 'RapelRiceAlwAmt'=>0, 
			'RapelPremiAmt'=>0, 'JHTEmpAmt'=>0, 'JKKAmt'=>0, 'JKMAmt'=>0, 'JHTCompAmt'=>0, 'JPKKEmpAmt'=>0, 'JPKKCompAmt'=>0 );//JPKKCompAmt
		$sql  = "SELECT RapelAmt, GrossAmt, OTAmt, UnpaidAmt, RapelRiceAlwAmt, RapelPremiAmt, JHTEmpAmt, "
			. "JKKAmt, JKMAmt, JHTCompAmt, JPKKEmpAmt,JPKKCompAmt "
			. "FROM rapeltrn WHERE EmpID='$empid' AND PrdMth='$prdmonth' AND FiscYear='$fiscyear'";
		$query= $this->db->query($sql);
		if($query->num_rows()>0){
			$row    = $query->result_array();
			$retval = $row[0];
		}
		return $retval;
	}
	
	function getPTKPMultp2Amt() {
		$sql = "SELECT PTKPAmt AS ptkpkone FROM ptkpmst  WHERE PTKPDesc='K1'";
		$query = $this->db->query($sql);
		$retval = $query->result_array();
		return $retval[0]['ptkpkone'];
	}
	//----------pph21--
	function getDistinctYears(){
		$sql = "SELECT FiscYear FROM payrolltrn GROUP BY FiscYear ORDER BY FiscYear DESC";
		$query = $this->db->query($sql);
		$retval = $query->result_array();
		return $retval;
	}

	function getDataPPH21($tahunbayar, $printall, $nourutdari, $nourutsampai){
		$sql = "SELECT
					p.EmpID, e.EmpName, e.NPWPNo, e.EmpAdd1, e.EmpAdd2, e.SexType, e.PTKPID, p.FiscYear,
					'' AS JobName,
					SUM(p.GrossAmt) AS GrossAmt,
					MIN(p.PrdMth) AS MulaiBulan,
					MAX(p.PrdMth) AS BulanAkhir,					
					SUM(p.TaxSupportAmt) AS TaxSupportAmt,
					SUM(p.JKKAmt) AS JKKAmt,
					SUM(p.JKMAmt) AS JKMAmt,
					SUM(p.JHTCompAmt) AS JHTCompAmt,
					SUM(p.JHTEmpAmt) AS JHTEmpAmt,
					SUM(p.JPKKEmpAmt) AS JPKKEmpAmt,
					SUM(p.JPKKCompAmt) AS JPKKCompAmt,
					SUM(p.OTAmt) AS OTAmt,
					SUM(p.AlwAmt) AS AlwAmt,
					SUM(p.DedAmt) AS DedAmt,
					SUM(p.RiceAlwAmt) AS RiceAlwAmt,
					SUM(p.DriverBonusAmt) AS DriverBonusAmt,
					SUM(p.UnpaidAmt) AS UnpaidAmt,
					SUM(p.RapelAmt) AS RapelAmt,
					SUM(p.PPHAmt) AS PPHAmt,
					SUM(p.DTPAmt) AS DTPAmt,
					pt.PTKPAmt,
					(SELECT SUM(a.GrossAmt) FROM payrolltrn a WHERE a.TransType ='T' AND a.FiscYear=$tahunbayar AND a.EmpID=p.EmpID GROUP BY a.EmpID) AS THR
				FROM payrolltrn p, empmst e, ptkpmst pt
				WHERE TransType ='G' AND p.FiscYear=$tahunbayar AND e.SPTExcluded=0 AND p.EmpID=e.EmpID AND e.PTKPID=pt.PTKPID
				GROUP BY p.EmpID ORDER BY e.EmpName ASC";
		if($printall < 1){ // klo yg diprint bukan semua pegawai(pake no urut)
			$jumlahdata = ($nourutsampai - $nourutdari) + 1;
			$mulai = $nourutdari -1;
			$sql .=" LIMIT $mulai,$jumlahdata";
		}
		$query = $this->db->query($sql);
		$retval = $query->result_array();
		return $retval;
	}

	function getCompanyData(){
		$sql = "SELECT CompanyName, NPWP, MillNPWP, TaxSignName, TaxSignNPWP FROM companymst";
		$query = $this->db->query($sql);
		$retval = $query->result_array();
		return $retval[0];
	}

	function deleterapel($condition){
		$this->db->delete('rapeltrn', $condition);
	}

	function insertrapel($datarapel) {
		$excludeempid	= array('');//orang2 yg pd saat KHL lebih besar, dari dia naik gol(KHT)=>minus rapel
		$empid			= $datarapel['EmpID'];
		if(($datarapel['RapelAmt']<>0)) {
			if (!(in_array($empid, $excludeempid))) {
				$this->db->insert('rapeltrn', $datarapel);
				return 1;
			}
		}
	}

	function replaceperiode($fiscyear, $prdmth, $startdate, $enddate){
		$condition = array(
			'FiscYear'	=> $fiscyear,
			'PrdMth'	=> $prdmth
		);
		$dataperiode = array(
			'FiscYear'			=> $fiscyear,
			'PrdMth'			=> $prdmth,
			'StartProcessDate'	=> $startdate,
			'EndProcessDate'	=> $enddate,
		);
		$this->db->delete('rapelperiodetrn', $condition);
		$this->db->insert('rapelperiodetrn', $dataperiode);
		return 1;
	}

	function getAllDedAmt($empid, $workingstartdate) {
		list($fiscyear, $prdmonth, $datetrn) = explode("-", $workingstartdate);
		$result[0]['AllDedAmt'] = 0;
		$sql="SELECT AllDedAmt FROM alldeddtltrn ad, alldedtrn a WHERE a.AllDedTransID=ad.AllDedTransID
				AND a.StatusFlg='1' AND a.FiscYear='$fiscyear' AND PrdMth='$prdmonth' AND ad.EmpID='$empid'";
		$query	= $this->db->query($sql);
		if($query->num_rows()>0 ) {
			$result = $query->result_array();
		}
		return $result[0]['AllDedAmt'];
	}

	function getBonusDriverAmt($empid, $workingstartdate) {
		list($fiscyear, $prdmonth, $datetrn) = explode("-", $workingstartdate);
		
		$result[0]['BonusDriverAmt'] = 0;
		$sql="SELECT
				(bd.PremiMati * bd.PremiMatiAmt) +
				(bd.NonInap1 * bd.NonInap1Amt) +
				(bd.NonInap2 * bd.NonInap2Amt) +
				(bd.Inap1 * bd.Inap1Amt) +
				(bd.Inap2 * bd.Inap2Amt)
					AS BonusDriverAmt
			FROM driverbonustrn b, driverbonusdtltrn bd
			WHERE b.ID=bd.ID AND bd.EmpID='$empid' AND b.StatusFlg='1'
				AND b.FiscYear='$fiscyear' AND b.PrdMth='$prdmonth'";
		$query	= $this->db->query($sql);
		if($query->num_rows()>0 ) {
			$result = $query->result_array();
		}
		return $result[0]['BonusDriverAmt'];
	}

	function getRitaseDriverAmt($empid, $workingstartdate) {
		list($fiscyear, $prdmonth, $datetrn) = explode("-", $workingstartdate);
		
		$result[0]['BonusRitaseAmt'] = 0;
		$sql="SELECT
					(rd.RitaseSiangQty * rd.RitaseSiangAmt) + (rd.RitaseMalamQty * rd.RitaseMalamAmt) + ( (rd.JangkosQty * rd.JangkosAmt)
					+ (rd.RitaseMingguQty * rd.RitaseMingguAmt)+ (rd.FFBQty * rd.FFBAmt) )
						AS BonusRitaseAmt
				FROM driverritasebonusdtltrn rd, driverritasebonustrn r
				WHERE r.ID=rd.ID AND rd.EmpID='$empid' AND r.StatusFlg='1'
					AND r.FiscYear='$fiscyear' AND r.PrdMth='$prdmonth'";
		$query	= $this->db->query($sql);
		if($query->num_rows()>0 ) {
			$result = $query->result_array();
		}
		return $result[0]['BonusRitaseAmt'];
	}
}
?>