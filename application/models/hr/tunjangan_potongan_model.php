<?php
class tunjangan_potongan_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
    }
    
	function view_data()//------------
	{
        $sql = "select * from alldedmst";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}

	function getMthFromCustomst(){
		$sql = "select PayrollProcessDate from customst";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row[0]['PayrollProcessDate'];
	}

	function alldedcatmst()//------
	{
		$sql    = "select * from alldedcatmst";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();
		
		return $row;
	}
	
	function data_edit($AllDedID)//----------
	{
        $sql = "select * from alldedmst where AllDedID = '$AllDedID'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}

	function getAlldedTrnList($num, $offset, $tanggal, $isnonkhl=TRUE){
		if($tanggal!=''){
			list($fiscyear, $prdmonthzero, $datetrn) = explode("-", $tanggal);
			$prdmonthzero = $prdmonthzero + 0;
		}		
		if($offset !='')
            $offset = $offset.',';
            
		$sql = "SELECT a.AllDedTransID, a.FiscYear, a.PrdMth, a.DivisionID, d.DivisionName
		FROM alldedtrn a, divisionmst d WHERE d.DivisionID=a.DivisionID ";
		if($tanggal!='')
			$sql .=" AND a.FiscYear='$fiscyear' AND a.PrdMth='$prdmonthzero'";
		$sql .=" ORDER BY FiscYear DESC, PrdMth DESC, AllDedTransID DESC LIMIT $offset $num";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
	}
	
	function getAlldedTrnListNumRows($tanggal, $isnonkhl=TRUE){
		if($tanggal!=''){
			list($fiscyear, $prdmonthzero, $datetrn) = explode("-", $tanggal);
			$prdmonthzero = $prdmonthzero + 0;
		}
		$sql = "SELECT AllDedTransID FROM alldedtrn a, divisionmst d
		WHERE d.DivisionID=a.DivisionID";
		if($tanggal!='')
			$sql .=" AND a.FiscYear='$fiscyear' AND a.PrdMth='$prdmonthzero'";
		$query = $this->db->query($sql);
        $result = $query->num_rows();
        return $result;
	}

	function getIsMasterOK($dataaldedtrn){
		$query = $this->db->get_where('alldedtrn', $dataaldedtrn);
		if($query->num_rows()>0) return FALSE;
		else return TRUE;
	}

	function divisionmst()
	{
		$sql    = "select * from divisionmst";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();
		
		return $row;
	}
			
	function activeempmst($iskhl=FALSE)
	{
		$sql    = "SELECT EmpID, CONCAT(EmpName,' - ', EmpID) AS EmpName FROM empmst WHERE";
		$sql .= "  ActiveFlg='1' ORDER BY EmpName ASC";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();
		
		return $row;
	}

	function getEmpDiv($divid){
		$sql    = "select EmpID, CONCAT(EmpName,' - ', EmpID) AS EmpName FROM empmst where";
		$sql .= "  ActiveFlg='1' AND DivisionID='$divid' ORDER BY EmpName ASC";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();

		return $row;
	}

	function empmst(){
		$sql    = "SELECT EmpID, CONCAT(EmpName,' - ', EmpID) AS EmpName FROM empmst ORDER BY EmpName";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();
		return $row;
	}

	function alldedmst(){
		$sql    = "SELECT AllDedID, AllDedName FROM alldedmst";
		$sql .= " ORDER BY AllDedName ASC";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();
		
		return $row;
	}

	function getAlldedtransLastIDPlusOne(){
		$sql    = "SELECT AllDedTransID + 1 AS ID FROM alldedtrn ORDER BY AllDedTransID DESC LIMIT 1";
		$qry    = $this->db->query($sql);
		$retval = 1;
		if($qry->num_rows()>0){
			$row    = $qry->result_array();
			$retval = $row[0]['ID'];
		}
		return $retval;
	}
	
	function getEmpID($tahun, $bulan, $tunpot){
		$sql    = "SELECT ad.EmpID FROM alldedtrn a, alldeddtltrn ad 
		WHERE a.AllDedTransID = ad.AllDedTransID AND PrdMth='$bulan' AND FiscYear='$tahun' AND AllDedID='$tunpot' ORDER BY EmpID ASC";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();
		return $row;
	}

	function getDataEdit($id){
		$sql    = "SELECT a.AllDedTransID, a.FiscYear, a.PrdMth, a.DivisionID, ad.EmpID, ad.AllDedID, ad.AllDedAmt
		FROM alldedtrn a, alldeddtltrn ad WHERE a.AllDedTransID='$id' AND a.AllDedTransID=ad.AllDedTransID ORDER BY ad.AllDedID, ad.EmpID";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();
		return $row;
	}
	
	function getMonthArray(){
		$month = array (
						array(
							'monthval'	=> 1,
							'monthname' => 'January'
						),
						array(
							'monthval'	=> 2,
							'monthname' => 'February'
						),
						array(
							'monthval'	=> 3,
							'monthname' => 'maret'
						),
						array(
							'monthval'	=> 4,
							'monthname' => 'April'
						),
						array(
							'monthval'	=> 5,
							'monthname' => 'Mei'
						),
						array(
							'monthval'	=> 6,
							'monthname' => 'Juni'
						),
						array(
							'monthval'	=> 7,
							'monthname' => 'Juli'
						),
						array(
							'monthval'	=> 8,
							'monthname' => 'Agustus'
						),
						array(
							'monthval'	=> 9,
							'monthname' => 'September'
						),
						array(
							'monthval'	=> 10,
							'monthname' => 'Oktober'
						),
						array(
							'monthval'	=> 11,
							'monthname' => 'November'
						),
						array(
							'monthval'	=> 12,
							'monthname' => 'Desember'
						)
		);
		return $month;
	}

	function dropIfExistTempTable(){
		$this->db->query("DROP TABLE IF EXISTS tunpottrn");
		$this->db->query("DROP TABLE IF EXISTS tunpotdtltrn");
	}

	function createTunpotTrnTempTable(){
		$sql =  "CREATE Temporary TABLE tunpottrn (AllDedTransID int(11), FiscYear int(11), PrdMth int(11),  DivisionID tinyint(4), UserID char(10), TransDate timestamp)";
		$this->db->query($sql);
	}

	function createTunpotDtlTempTable(){
		$sql =	"CREATE Temporary TABLE tunpotdtltrn (ID bigint(12), AllDedTransID  int(11), EmpID char(12), AllDedID tinyint(4),
				AllDedAmt decimal(9,0))";
		$this->db->query($sql);
	}

	function saveOldAldedtrn($tunpotid){
		$sql =	"INSERT INTO tunpottrn SELECT * FROM alldedtrn WHERE AllDedTransID='$tunpotid'";
		$this->db->query($sql);
	}

	function saveOldAldedDtltrn($tunpotid){
		$sql =	"INSERT INTO tunpotdtltrn SELECT * FROM alldeddtltrn WHERE AllDedTransID='$tunpotid'";
		$this->db->query($sql);
	}

	function rollbackFromTempTables($tunpotid){
		$this->db->delete('alldedtrn', array('AllDedTransID' => $tunpotid));

		$sql1 =	"INSERT INTO alldedtrn SELECT * FROM tunpottrn WHERE AllDedTransID='$tunpotid'";
		$this->db->query($sql1);

		$sql2 =	"INSERT INTO alldeddtltrn SELECT * FROM tunpotdtltrn WHERE AllDedTransID='$tunpotid'";
		$this->db->query($sql2);
	}
}
?>