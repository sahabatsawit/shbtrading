<?php
class pekerja_model extends CI_Model {
	
	function __construct() {
		parent::__construct();
	}
 	
	function view_data($num, $offset, $keyword, $keywordurl2) {
		if($offset !='')
            $offset = $offset.',';
		$sql = "select e.EmpID, e.EmpName, d.DivisionID, d.DivisionName, e.BankID, e.BankTransferFlg,
				(select b.BankName from bankref b where b.BankID=e.BankID) AS NamaBank
				from empmst e, divisionmst d
				where e.DivisionID = d.DivisionID";
		if($keyword !='')
			$sql .= " AND $keywordurl2 like '%$keyword%'";

		$sql .= " LIMIT $offset $num";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		
		return $row;
    }

	function getNumRowsPekerja($keyword, $keywordurl2) {
        $sql = "select e.EmpID
				from empmst e, divisionmst d
				where e.DivisionID = d.DivisionID";
		if($keyword !='')
			$sql .= " AND $keywordurl2 like '%$keyword%'";
		
        $query = $this->db->query($sql);
        return $query->num_rows();
	}
	
	function LastEmpID($JoinDate, $action) {
		$sql       = "select LastEmpID from customst";
		$qry       = $this->db->query($sql);
		$row       = $qry->result_array();
		$LastEmpID = $row[0]['LastEmpID'];
		
		$CutDate   = substr($JoinDate, 0, 4).substr($JoinDate, 5, 2);
		$num       = substr($LastEmpID, -5, 5) + 1;
		$numResult = substr('000000'.$num, -6, 6);
		$LastEmpID = $CutDate.$numResult;
		
		if($action == 'Get'){
			echo $LastEmpID;
		}else{
			return $LastEmpID;
		}
    }
	
    function religionmst() {
		$sql    = "select * from religionmst";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();
		
		return $row;
    }
	
	function getemlplocationmst() {
		$sql    = "select * from emplocationmst";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();
		
		return $row;
	}
	
    function ptkpmst() {
		$sql    = "select PTKPID, PTKPDesc from ptkpmst";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();
		
		return $row;
    }
    
    function educationmst() {
		$sql    = "select * from educationmst";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();
		
		return $row;
    }
    
    function divisionmst() {
		$sql    = "select * from divisionmst";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();
		
		return $row;
    }
    
    function employmentmst() {
		$sql    = "select EmploymentID, EmploymentName from employmentmst";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();
		
		return $row;
    }
    
    function jobmst() {
		$sql    = "select JobID, JobName from jobmst ORDER BY JobName ASC";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();
		return $row;
    }
	
    function bankmst() {
		$sql    = "select BankID, BankName from bankref";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();
		
		return $row;
    }

    function checkrollmst() {
        $sql = "SELECT * FROM checkrollmst ORDER BY CheckrollName ASC";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function healthinsurmst() {
		$sql = "SELECT ID, InsuranceName FROM healthinsurmst";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
	}
    
    function data_edit($EmpID) {
		$sql = "select * from empmst where EmpID = '$EmpID'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
    }
	
	function data_skdtail($EmpID) {
		$emptyval	= array(
							array(
								'ID'		=> '0',
								'SKDate'	=> '',
								'SKNo'		=> '',
								'SKJobID'	=> ''
							)
						);
		$sql = "select ID, SKDate, SKNo, SKJobID from empskmst where EmpID = '$EmpID'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		$retval	=(count($row)>0)?$row:$emptyval;
		return $retval;
	}
}
?>