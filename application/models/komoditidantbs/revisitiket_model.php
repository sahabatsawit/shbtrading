<?php
class RevisiTiket_model extends CI_Model {
	
    function __construct() {
        parent::__construct();
    }

	function getProductTiket($notiket) {		
		$sql	= "SELECT cpo.DocketDate, cpo.InOutFlg, cpo.ProductID, cst.WbDate "
				. "FROM cpowbtrn cpo, customst cst "
				. "WHERE cpo.DocketID='$notiket' "
				. "UNION ALL "
				. "SELECT tbs.DocketDate, tbs.InOutFlg, tbs.ProductID, cst.WbDate "
				. "FROM ffbwbtrn tbs, customst cst "
				. "WHERE tbs.DocketID='$notiket'";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		if(isset($result[0])) {
			$wbdate		= $result[0]['WbDate'];
			$docketdate	= $result[0]['DocketDate'];
			$isclosed	= ($wbdate!=$docketdate)?1:0;
			$result[0]['IsClosed']	= $isclosed;
		}
		return $result;
	}
	
	function getDataCpowbtrn($notiket) {
		$sql	= "SELECT c.DocketID,  c.DocketDate, c.TruckID, c.ContractorID, c.ProductID, "
				. "c.StorageID, c.CustomerID, cs.CustomerName, "
				. "DATE_FORMAT( c.TripStartTime ,  '%H:%i' ) AS TripStartTime, DATE_FORMAT( c.TimeIn ,  '%H:%i' ) AS TimeIn, "
				. "DATE_FORMAT( c.TimeOut ,  '%H:%i' ) AS TimeOut, c.SPBDate, c.OrderDate, "
				. "c.GrossWgt, c.TareWgt, c.AcidPtg,  c.WaterPtg,c.DirtyPtg, c.BrokenKernelPtg, c.DriverName, c.LicenseNumber, c.InOutFlg, "
				. "c.InWBID, c.OutWBID, c.PrintCnt, c.ManualFlg, c.UserID, c.TransDate "
				. "FROM cpowbtrn c, customermst cs "
				. "WHERE c.DocketID='$notiket' AND c.CustomerID=cs.CustomerID";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return isset($result[0])?$result[0]:$result;
	}
	
	function getDataCpowbdtldotrn($notiket) {
		$sql	= "SELECT c.DocketID,  c.DOID, c.SPBID, c.OrderID, c.NetWeight, d.DOOrderID, "
				. "c.QtyContractRemain, ct.ContractID, ct.ContractNo, dc.ContractQty, "
				. "cn.ContractorID, cn.ContractorName, cs.CustomerID, cs.CustomerName "
				. "FROM cpowbdtldotrn c, domst d, contractmst ct, cpowbtrn cp, "
				. "docontractordtlmst dc, contractormst cn, customermst cs "
				. "WHERE d.DOID=c.DOID AND d.DOID=dc.DOID AND c.DocketID=cp.DocketID AND dc.ContractorID=cp.ContractorID AND cn.ContractorID=dc.ContractorID "
				. "AND ct.ContractID=d.ContractID AND cs.CustomerID=ct.CustomerID AND c.DocketID='$notiket'";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		$nodataarr	= array(
			'DocketID'			=> $notiket,
			'DOID'				=> '',
			'SPBID'				=> '',
			'OrderID'			=> '',
			'NetWeight'			=> '0',
			'DOOrderID'			=> '',
			'QtyContractRemain'	=> '',
			'ContractID'		=> '',
			'ContractNo'		=> '',
			'ContractQty'		=> '',
			'ContractorID'		=> '',
			'ContractorName'	=> '',
			'CustomerID'		=> '',
			'CustomerName'		=> ''
		);
		return isset($result[0])?$result[0]:$nodataarr;
	}
	
	function getDataCpoSeal($notiket) {
		$nodataarr	= array(
			array(
				'DocketID'	=> $notiket,
				'SealNo'	=> ''
			)
		);
		$sql	= "SELECT DocketID,  SealNo "
				. "FROM cpowbdtlsealtrn "
				. "WHERE DocketID='$notiket'";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return isset($result)?$result:$nodataarr;
	}
	
	function getCPOTruckmst($notiket, $iscpo=TRUE) {
		$sql	= "SELECT t.TruckID "
				. "FROM cpowbtrn c, truckmst t "
				. "WHERE c.DocketID='$notiket' AND t.TruckID != c.TruckID ";
		if($iscpo)
			$sql	.= " AND t.ProductID=c.ProductID";
		else {
			$sql	.= " AND t.ProductID!=1";
		}
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result;
	}
	
	function getDataTruckIDChangeAjx($truckid) {
		$sql	= "SELECT t.TruckID, t.DriverName, t.LicenseNumber "
				. "FROM truckmst t "
				. "WHERE t.TruckID = '$truckid'";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result[0];
	}
	
	function getDOmst($notiket) {
		
		$sql	= "SELECT d.DOID, d.DOOrderID, (dc.ContractQty - dc.UsedQty) AS Sisa "
				. "FROM cpowbdtldotrn cd, domst d, cpowbtrn c, docontractordtlmst dc "
				. "WHERE cd.DocketID='$notiket' AND cd.DocketID=c.DocketID "
				. "AND d.DOID != cd.DOID AND d.ProductID=c.ProductID "
				. "AND d.DOID=dc.DOID AND (dc.ContractQty - dc.UsedQty) > 0 "
			. "ORDER BY Sisa DESC";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		if(isset($result[0])) {
			$retval	= $result;
		} else {
			$sql2	= "SELECT d.DOID, d.DOOrderID, (dc.ContractQty - dc.UsedQty) AS Sisa "
				. "FROM domst d, cpowbtrn c, docontractordtlmst dc "
				. "WHERE c.DocketID='$notiket' AND c.CustomerID=d.CustomerID "
				. "AND d.ProductID=c.ProductID "
				. "AND d.DOID=dc.DOID AND d.DOFromDate < c.DocketDate "
			. "ORDER BY d.DOFromDate";
			$query2	= $this->db->query($sql2);
			$result2	= $query2->result_array();
			$retval	= $result2;
		}
		return $retval;
	}
	
	function getDataDOIDChangeAjx($doid) {
		$sql	= "SELECT d.DOID, d.DOOrderID, c.ContractNo, d.ContractID, ctr.ContractorName, "
				. "cs.CustomerName, cs.CustomerID, c.ContractQty, dc.ContractorID, "
			. "(c.ContractQty -  ( SELECT SUM(cpd.NetWeight) "
			. "						FROM cpowbdtldotrn cpd, cpowbtrn cp "
			. "						WHERE cpd.DOID=dc.DOID AND cp.DocketID=cpd.DocketID AND inoutflg='o' "
			. "						)"
			. ") AS QtyContractRemain "
				. "FROM domst d, docontractordtlmst dc, contractmst c, customermst cs, contractormst ctr "
				. "WHERE d.DOID='$doid' AND d.DOID = dc.DOID AND c.ContractID=d.ContractID "
				. "AND cs.CustomerID=c.CustomerID AND ctr.ContractorID=dc.ContractorID";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result[0];
	}
	
	function getTotalAngkut($doid) {
		$sql	= "SELECT SUM(cpd.NetWeight) AS TotalAngkut "
			. "FROM cpowbdtldotrn cpd, cpowbtrn cp "
			. "WHERE cpd.DOID='$doid' AND cp.DocketID=cpd.DocketID AND cp.InOutFlg='o'";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return isset($result[0]['TotalAngkut'])?$result[0]['TotalAngkut']:0;
	}
	
	function getDataffbwbtrn($notiket) {
		$sql	= "SELECT DocketID,  DocketDate, TruckID, f.ContractorID, c.ContractorName, ProductID, "
				. "DATE_FORMAT( TripStartTime ,  '%H:%i' ) AS TripStartTime, "
			. "DATE_FORMAT( TimeIn ,  '%H:%i' ) AS TimeIn, SortasiWgt, SortasiPtg, "
				. "DATE_FORMAT( TimeOut ,  '%H:%i' ) AS TimeOut, SPBDate, "
				. "GrossWgt, TareWgt, (GrossWgt - TareWgt) AS Berat, DriverName, InOutFlg, PaidFlg, PaidDate, "
			. "(GrossWgt - TareWgt) - SortasiWgt AS NettoBayar, "
				. "InWBID, OutWBID, PrintCnt, ManualFlg, f.UserID, f.TransDate "
				. "FROM ffbwbtrn f, contractormst c "
				. "WHERE DocketID='$notiket' AND f.ContractorID=c.ContractorID";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return isset($result[0])?$result[0]:$result;
	}
	
	function getDataffbwbdtltrn($notiket) {
		$sql	= "SELECT f.DocketID,  f.ID_kudgroupareamst, k.KUDName, f.AreaID, a.AreaName, f.BlockID, f.FFBCnt, "
				. "CEIL(f.FFBWeight) AS FFBWeight, f.FFBGrossWgt, f.FFBTareWgt, f.FFBSortasiWgt, f.LFWeight "
				. "FROM ffbwbdtltrn f, kudgroupareamst k, areamst a "
				. "WHERE f.DocketID='$notiket' AND f.ID_kudgroupareamst=k.ID AND a.AreaID=f.AreaID";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		
		$nodataarr	= array(
			'DocketID'				=> $notiket,
			'ID_kudgroupareamst'	=> '',
			'KUDName'				=> '',
			'AreaID'				=> '',
			'AreaName'				=> '',
			'BlockID'				=> '',
			'FFBCnt'				=> '',
			'FFBWeight'				=> '',
			'FFBGrossWgt'			=> '',
			'FFBTareWgt'			=> '',
			'FFBSortasiWgt'			=> '',
			'LFWeight'				=> ''
		);
		return isset($result[0])?$result[0]:$nodataarr;
	}
	
	
	function getffbTruckmst($notiket) {
		$sql	= "SELECT t.TruckID "
				. "FROM truckmst t, ffbwbtrn f "
				. "WHERE f.DocketID='$notiket' AND t.ProductID != 1 AND t.TruckID != f.TruckID";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result;
	}
	
	function getkudgroupmst($notiket) {
		$sql	= "SELECT k.ID, k.KUDName "
				. "FROM ffbwbdtltrn f, kudgroupareamst k "
				. "WHERE f.DocketID='$notiket' AND f.ID_kudgroupareamst != k.ID ";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		if(isset($result[0])) {
			$retval	= $result;
		} else {
			$sql2	= "SELECT k.ID, k.KUDName "
				. "FROM kudgroupareamst k ";
			$query2	= $this->db->query($sql2);
			$result2	= $query2->result_array();
			$retval	= $result2;
		}
		return $retval;
	}
	
	function getareamst($notiket) {
		$sql	= "SELECT a.AreaID, a.AreaName "
				. "FROM ffbwbdtltrn f, areamst a "
				. "WHERE f.DocketID='$notiket' AND f.AreaID != a.AreaID "
			. "ORDER BY a.AreaName";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		if(isset($result[0])) {
			$retval	= $result;
		} else {
			$sql2	= "SELECT a.AreaID, a.AreaName "
				. "FROM areamst a ORDER BY a.AreaName";
			$query2	= $this->db->query($sql2);
			$result2	= $query2->result_array();
			$retval	= $result2;
		}
		return $retval;
	}
}
?>
