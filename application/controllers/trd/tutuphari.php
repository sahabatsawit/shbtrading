<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);

class TutupHari extends Authcontroller {
    /**
	 *
	 */
    var $isusermodify;
    
   	function __construct() {
        parent::__construct();
        define("MENU_ID", "109");
        $userid = $this->session->userdata('UserID'); 
        $this->redirectNoAuthRead($userid,MENU_ID);
        $this->isusermodify = $this->isUserAuthModify($userid,MENU_ID);
    }
	
	function index() {
		$this->load->helper('text');		
		$data['wbdate']	= $this->_getWBDate();
		$this->load->view('trading/bukatutuphari/tutuphari_index', $data);
	}
	
	function tutupproc() {
		$submit	= $this->input->post('submit');
		$tglwb	= $this->input->post('tglwb');
		
		if($submit=='PROSES') {
			list($wbyr, $wbmth, $wbday)	= explode('-',$tglwb);
			
			$this->db->trans_start();//-----------------------------------------------------START TRANSAKSI
			
			$sql	= "DELETE FROM ffbmutationtrn WHERE FfbDate='$tglwb'";
			$this->db->query($sql);
			
			if($tglwb=='2015-08-05'){
				$sql2	= "ALTER TABLE ffbmutationtrn MODIFY StartWgt INT(10) NOT NULL";
				$this->db->query($sql2);
			}
			
			$tglkemarin	= date("Y-m-d", mktime(0, 0, 0, $wbmth+0, $wbday-1, $wbyr));			
			$anggota_arrs	= $this->_getAnggota();
			foreach ($anggota_arrs as $anggota_arr) {
				$anggota		= $anggota_arr['KUDMemberID'];
				
				$startwgt		= $this->_getStartWgt($tglkemarin, $anggota);
				$ffbinpaywgt	= $this->_getFfbInPayWgt($tglwb, $anggota);
				$ffbinwgt		= $this->_getFfbInWgt($tglwb, $anggota);
				$ffboutwgt		= $this->_getFfbOutWgt($tglwb, $anggota);
				$receivedwgt	= $this->_getReceivedWgt($tglwb, $anggota);				
				
				$dataffbmutation	= array(
										'FfbDate'		=> $tglwb,
										'KUDMemberID'	=> $anggota,
										'StartWgt'		=> $startwgt,
										'FfbInPayWgt'	=> $ffbinpaywgt,
										'FfbInWgt'		=> $ffbinwgt,
										'FfbOutWgt'		=> $ffboutwgt,
										'ReceivedWgt'	=> $receivedwgt
									);
				$this->db->insert('ffbmutationtrn', $dataffbmutation);
			}
			
			$tgltutup= date("Y-m-d", mktime(0, 0, 0, $wbmth+0, $wbday+1, $wbyr));
			list($wbyr_a, $wbmth_a, $wbday_tutup)	= explode('-',$tgltutup);
			$data['WBDate']= $tgltutup;
			if($wbday_tutup=="01") {
				$thnbln	= date("ym", mktime(0, 0, 0, $wbmth+0, $wbday+1, $wbyr));
				$dataticket['LastDONo']= $thnbln.'0000';	
				$this->db->update('companymst', $dataticket, array('CompanyID'	=> 1));
			}
			$this->db->update('customst', $data, array('ID'	=> 1));
			
			$this->db->trans_complete();//----------------------------------------------------END TRANSAKSI
		}
		redirect('trd/tutuphari');
	}
	
	function _getReceivedWgt($tglwb, $anggota) {
		$sql = "SELECT SUM(fb3.ReceivedWgt) AS ReceivedWgt "
			. "FROM ffbwbtrn fb3 "
			. "WHERE fb3.DocketDate='$tglwb' AND fb3.IsSell=1 AND fb3.KUDMemberID=$anggota "
			. "GROUP BY fb3.DocketDate";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		$retval	= 0;
		if(isset($result[0])) {
			$retval	= $result[0]['ReceivedWgt'] + 0;
		}
		return $retval;
	}
	
	function _getFfbOutWgt($tglwb, $anggota) {
		$sql = "SELECT SUM(fb.GrossWgt) AS GrossWgt, SUM(fb.TareWgt) AS TareWgt "
			. "FROM ffbwbtrn fb "
			. "WHERE fb.DocketDate='$tglwb' AND fb.IsSell=1 "
			. " AND fb.KUDMemberID=$anggota "
			. "GROUP BY fb.DocketDate";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		$retval	= 0;
		if(isset($result[0])) {
			$GrossWgt	= $result[0]['GrossWgt'];
			$TareWgt	= $result[0]['TareWgt'];
			$retval		= $GrossWgt - $TareWgt;
		}
		return $retval;
	}
	
	function _getFfbInWgt($tglwb, $anggota) {
		$sql = "SELECT SUM(fb.GrossWgt) AS GrossWgt, SUM(fb.TareWgt) AS TareWgt "
			. "FROM ffbwbtrn fb "
			. "WHERE fb.DocketDate='$tglwb' AND fb.IsSell=0 "
			. " AND fb.KUDMemberID=$anggota "
			. "GROUP BY fb.DocketDate";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		$retval	= 0;
		if(isset($result[0])) {
			$GrossWgt	= $result[0]['GrossWgt'];
			$TareWgt	= $result[0]['TareWgt'];
			$retval		= $GrossWgt - $TareWgt;
		}
		return $retval;
	}
	
	function _getStartWgt($tglkemarin, $anggota) {
		$sql = "SELECT fm.StartWgt, fm.FfbInWgt, fm.FfbOutWgt FROM ffbmutationtrn fm WHERE fm.FfbDate='$tglkemarin' AND fm.KUDMemberID=$anggota";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		$retval	= 0;
		if(isset($result[0])) {
			$StartWgt	= $result[0]['StartWgt'];
			$FfbInWgt	= $result[0]['FfbInWgt'];
			$FfbOutWgt	= $result[0]['FfbOutWgt'];
			$retval		= $StartWgt + ($FfbInWgt - $FfbOutWgt);
		}
		return $retval;		
	}
	
	function _getFfbInPayWgt($tglwb, $anggota) {
		$sql = "SELECT SUM(fb.GrossWgt) AS GrossWgt, SUM(fb.SortasiWgt) AS SortasiWgt, SUM(fb.TareWgt) AS TareWgt "
			. "FROM ffbwbtrn fb "
			. "WHERE fb.DocketDate='$tglwb' AND fb.IsSell=0 "
			. " AND fb.KUDMemberID=$anggota "
			. "GROUP BY fb.DocketDate";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		$retval	= 0;
		if(isset($result[0])) {
			$GrossWgt	= $result[0]['GrossWgt'];
			$SortasiWgt	= $result[0]['SortasiWgt'];
			$TareWgt	= $result[0]['TareWgt'];
			$retval		= $GrossWgt - ($SortasiWgt + $TareWgt);
		}
		return $retval;
	}
	
	function _getWBDate() {
		$sql = "SELECT WBDate FROM customst";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return  $result[0]['WBDate'];
	}
	
	function _getAnggota() {
		$sql = "SELECT KUDMemberID FROM kudmembermst";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return  $result;
	}
	
	function test($tglwb) {		
		list($wbyr, $wbmth, $wbday)	= explode('-',$tglwb);	
			$tgltutup= date("Y-m-d", mktime(0, 0, 0, $wbmth+0, $wbday-1, $wbyr));
			print_array($tgltutup);
	}
} 