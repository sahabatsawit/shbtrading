<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class LapTBSKeluar extends Authcontroller {
	
    var $isusermodify;
	
	function __construct() {
		parent::__construct();
        define("MENU_ID", "112");
        $userid = $this->session->userdata('UserID'); 
        $this->redirectNoAuthRead($userid,MENU_ID);
        $this->isusermodify = $this->isUserAuthModify($userid,MENU_ID);
	}
	  
	function index() {
	 	$data['customermst'] = $this->_customer();
		$this->load->view('trading/laporan/laptbskeluar_index', $data);
	}
	
	function _customer() {
		$sql	= "SELECT  CustomerID, CustomerName FROM customermst ORDER BY CustomerName";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result;
	}
	
	function cetak() {
	    $from = $this->input->post('from');
	    $until = $this->input->post('until');
	    $customerid = $this->input->post('CustomerID');
		
		$qry = "SELECT
					a.CustomerName,
					k.KUDMemberName,
					h.DocketDate,
					h.DocketID,
					TIME_FORMAT(h.TimeOut, '%H:%i') AS TimeOut,
					h.TruckID,
					(h.GrossWgt - h.TareWgt) AS Netto,
					h.PriceAmt,
					h.ReceivedWgt,
					h.ReceivedWgt * h.PriceAmt AS BayarRp
				FROM ffbwbtrn h, customermst a, kudmembermst k
				WHERE h.DocketDate BETWEEN '$from' AND '$until' 
					AND h.ProductID = '2' AND h.IsSell=1 AND h.KUDMemberID=k.KUDMemberID
					AND a.CustomerID=h.CustomerID ";
			if($customerid != 'all'){
				$qry	.= "AND a.CustomerID='$customerid' ";
			}
		$qry .= "HAVING Netto <> 0
				ORDER BY a.CustomerName, k.KUDMemberName, h.DocketDate, h.DocketID";
	    
		if($this->input->post('submit') == 'PDF') {
			$this->load->library('PDF_MYSQL_Table');
			$pdf = new PDF_MySQL_Table();
			$pdf->Open();
			$pdf->PageOrientation('L');
			$pdf->AddPage();			
			$pdf->SetTitle("TBS Keluar");
			$pdf->AddCriteria("Tanggal ", 10, $from. " s/d " . $until , 50);
					
			$aSubTotal = array(array('CustomerName'=>0,'KUDMemberName'=>0,'DocketDate'=>0,'DocketID'=>0, 'TimeOut'=>0, 'TruckID'=>0,
				'PriceAmt'=>0,'Netto'=>0,'ReceivedWgt'=>0,'BayarRp'=>0));
			
			$aGrandTotal = array('CustomerName'=>0,'KUDMemberName'=>0,'DocketDate'=>0,'DocketID'=>0, 'TimeOut'=>0, 'TruckID'=>0,
				'PriceAmt'=>0,'Netto'=>0,'ReceivedWgt'=>0,'BayarRp'=>0);
			$pdf->SetGrandTotal($aGrandTotal);
			
			$pdf->SetSubTotalBy($aSubTotal);
			$pdf->AddCol("CustomerName",			36, "Pelanggan",			"L", 1, -1);
			$pdf->AddCol("KUDMemberName",			30, "Anggota KUD",					"L", 0, -1);
			$pdf->AddCol("DocketDate",				16, "Tgl",					"L", 0, -1);
			$pdf->AddCol("DocketID",				15, "Ticket",				"L", 0, -1);
			$pdf->AddCol("TimeOut",					10, "Out",					"L", 0, -1);
			$pdf->AddCol("TruckID",					18, "Truck",				"L", 0, -1);
			$pdf->AddCol("Netto",					12, "Netto",				"R", 0, 0, 'EN', 0);			
			$pdf->AddCol("PriceAmt",				13, "Harga",				"R", 0, 0, 'EN', 0);
			$pdf->AddCol("ReceivedWgt",				20, "Diterima(Kg)",				"R", 0, 0, 'EN', 0);
			$pdf->AddCol("BayarRp",					20, "Bayar(Rp)",				"R", 0, 0, 'EN', 0);
			
			$pdf->Table($qry);
			$pdf->output();
		} else {
			$this->load->library('ExportToCSV');
			$export = new ExportToCSV();
			$export->SetQuery($qry);
			$export->Export();
		}	
	}
}