<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);

class BukaHari extends Authcontroller {
    /**
	 *
	 */
    var $isusermodify;
    
   	function __construct() {
        parent::__construct();
        define("MENU_ID", "110");
        $userid = $this->session->userdata('UserID'); 
        $this->redirectNoAuthRead($userid,MENU_ID);
        $this->isusermodify = $this->isUserAuthModify($userid,MENU_ID);
    }
	
	function index() {
		$this->load->helper('text');		
		$data['wbdate']	= $this->_getWBDate();
		$this->load->view('trading/bukatutuphari/bukahari_index', $data);
	}
	
	function bukaproc() {
		$submit		= $this->input->post('submit');
		$tglbuka	= $this->input->post('tglbuka');		
		
		if($submit=='PROSES') {			
			$this->db->trans_start();//-----------------------------------------------------START TRANSAKSI
			
			$data	= array(
							'WBDate'	=> $tglbuka
					);
			$this->db->update('customst', $data, array('ID'	=> 1));
			
			$this->db->trans_complete();//----------------------------------------------------END TRANSAKSI
		}
		redirect('trd/bukahari');
	}
	
	function _getWBDate() {
		$sql = "SELECT WBDate FROM customst";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return  $result[0]['WBDate'];
	}
	
	function test() {		
		print_array();
	}
} 