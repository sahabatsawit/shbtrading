<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);

class TbsMasuk extends Authcontroller {
    /**
	 *
	 */
    var $isusermodify;
    
   	function __construct() {
        parent::__construct();
        define("MENU_ID", "101");
        $userid = $this->session->userdata('UserID'); 
        $this->redirectNoAuthRead($userid,MENU_ID);
        $this->isusermodify = $this->isUserAuthModify($userid,MENU_ID);
    }
	
	function index() {
		$this->load->library('pagination');
		$this->load->helper('text');
		$urisegment	= 6;
		$keyword	= $this->uri->segment($urisegment -2);
		$keywordurl = $keyword;
		$keywordurl2 = $this->uri->segment($urisegment -1);

		if(($keyword=='') || ($keyword=='nokeyword')){
				$keyword	= '';
				$keywordurl = 'nokeyword';
				$keywordurl2 = 'nokeyword';
		}
        if($this->input->post('submit')=='Cari'){
			$keyword    = $this->input->post('optionValue');
			$keywordurl2 = $this->input->post('option');
			if($keyword ==''){
				$keywordurl = 'nokeyword';
				$keywordurl2 = 'nokeyword';
			}
			else{
				$keywordurl = $keyword;
			}
		}
		$dataperpage				= 11;
		$config['per_page']         = $dataperpage;
		$config['base_url']         = site_url()."/trd/tbsmasuk/index/$keywordurl/$keywordurl2/";
		$config['uri_segment']      = $urisegment;
        $config['total_rows']       = $this->_getNumRowsItem($keyword, $keywordurl2);

        $this->pagination->initialize($config);
		$fromurisegment				= $this->uri->segment($urisegment);
		$data['view_data']			= $this->_view_data($dataperpage, $fromurisegment, $keyword, $keywordurl2);
		$data['wbdate']				= $this->_getWBDate();
		$this->load->view('trading/tbsmasuk/tbsmasuk_index', $data);
	}
	
	function edit($docketid) {
		$this->load->helper('text');
		$data['areamst']		= $this->_getAreaList();
		$data['contractormst']	= $this->_getContractorList();
		$data['kudmembermst']	= $this->_getKUDMemberList();
		$data['datatbs_arr']	= $this->_getTiketTBS($docketid);
		$data['datadtltbs_arr']	= $this->_getFfbDtlTrnDB($docketid);
		$data['urlsegment']		= $this->uri->uri_string();
		$this->load->view('trading/tbsmasuk/tbsmasuk_edit', $data);
	}
	
	function editproc($docketid) {
		$submit			= $this->input->post('submit');
		$nokendaraan	= $this->input->post('noregkendaraan');
		$driver			= $this->input->post('namadriver');
		$tgldocket		= $this->input->post('tgltiket');
		$contractorid	= $this->input->post('angkutan');
		$spbdate		= $this->input->post('tglspb');
		$spbno			= $this->input->post('nospb');
		$jammasuk		= $this->input->post('timein').":00";
		$harga			= $this->input->post('harga');
		$sortasiptg		= $this->input->post('sortasiptg');
		$sortasiberat	= $this->input->post('sortasikg');
		$beratbayar		= $this->input->post('nettobayarkg');		
		$beratmasuk		= $this->input->post('beratkg');
		$tarra			= $this->input->post('tarra');
		$userid			= $this->session->userdata('UserID');		
		$kudmemberid	= $this->input->post('kudmemberid');
		$area			= $this->input->post('area');
		$blok			= $this->input->post('areablok');
		$tandan			= $this->input->post('jumltandan');
		
		
		if($submit=='SIMPAN') {			
			$this->db->trans_start();//-----------------------------------------------------START TRANSAKSI
			//-------------------- special case ------------
			if($docketid=='15100852') {
				$tgldocket = '2015-10-17';
			}
			//--------------------------------------eo special case----
			$datadocket	= array(							
							'DocketDate'	=> $tgldocket,
							'TruckID'		=> $nokendaraan,
							'ContractorID'	=> $contractorid,
							'ProductID'		=> '2',
							'TimeIn'		=> $jammasuk,
							'SPBID'			=> $spbno,
							'SPBDate'		=> $spbdate,
							'LFWgt'			=> 0,
							'GrossWgt'		=> $beratmasuk,
				'KUDMemberID'	=> $kudmemberid,
							'TareWgt'		=> $tarra,
							'SortasiWgt'	=> $sortasiberat,
							'SortasiPtg'	=> $sortasiptg,
							'DriverName'	=> $driver,
							'PriceAmt'		=> $harga,
							'InOutFlg'		=> 'o',
							'InWBID'		=> $userid,
							'OutWBID'		=> $userid,
							'ManualFlg'		=> 1,
							'UserID'		=> $userid
					);
			$this->db->update('ffbwbtrn', $datadocket, array('DocketID'	=> $docketid));

			$datatbs	= array(							
							'AreaID'		=> $area,
							'BlockID'		=> $blok,
							'FFBCnt'		=> $tandan,
							'FFBWeight'		=> $beratbayar,
							'FFBGrossWgt'	=> $beratmasuk,
							'FFBTareWgt'	=> $tarra,
							'FFBSortasiWgt'	=> $sortasiberat,
							'LFWeight'		=> 0
						);
			$this->db->update('ffbwbdtltrn', $datatbs, array('DocketID'	=> $docketid));
			
			$this->db->trans_complete();//----------------------------------------------------END TRANSAKSI
		}
		
		// back to page asal	
		$urlstring	= $this->input->post('urlsegment');
		$urlarr = explode("/", $urlstring);
		$url	= '';
		$i	= 0;
		foreach ($urlarr as $uri) {
			if($i>5)
				$url	.= '/'.$uri;
			$i++;
		}
		redirect('trd/tbsmasuk'.$url);
	}
	
	function lihat($docketid) {
		$this->load->helper('text');
		$data['areamst']		= $this->_getAreaList();
		$data['contractormst']	= $this->_getContractorList();
		$data['kudmembermst']	= $this->_getKUDMemberList();
		$data['datatbs_arr']	= $this->_getTiketTBS($docketid);
		$data['datadtltbs_arr']	= $this->_getFfbDtlTrnDB($docketid);
		$data['urlsegment']		= $this->uri->uri_string();
		$this->load->view('trading/tbsmasuk/tbsmasuk_lihat', $data);
	}
	
	function input() {
		$data['wbdate']			= $this->_getWBDate();
		$data['areamst']		= $this->_getAreaList();
		$data['contractormst']	= $this->_getContractorList();
		$data['kudmembermst']	= $this->_getKUDMemberList();
		$this->load->view('trading/tbsmasuk/tbsmasuk_input', $data);
	}
	
	function getBlok_ajx($areaid) {
		$sql	= "SELECT  BlockID FROM blockmst ";
		$sql .= "WHERE AreaID='$areaid' ";
		$sql .= "ORDER BY BlockID";
		$query	= $this->db->query($sql);
		$blocks	= $query->result_array();
		
		foreach ($blocks as $block) {
			$blokid	= $block['BlockID'];
			echo "\n<option value='".$blokid."'>".$blokid."</option>";
		}
	}
	
	function getDataTruck_ajx($truckid) {
		$truckid	= urldecode($truckid);
		
		$sql = "SELECT ContractorID, DriverName, LicenseNumber, TruckTare, TruckID AS data "
			. "FROM truckmst "
			. "WHERE ProductID=2 AND TruckID LIKE '%$truckid%' LIMIT 40";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		echo json_encode($row);
    }
	
	function inputproc() {
		$submit			= $this->input->post('submit');
		$nokendaraan	= $this->input->post('noregkendaraan');
		$driver			= $this->input->post('namadriver');
		$tgldocket		= $this->input->post('tgltiket');
		$contractorid	= $this->input->post('angkutan');
		$spbdate		= $this->input->post('tglspb');
		$spbno			= $this->input->post('nospb');
//		$tripstart		= $this->input->post('tripstart').":00";
		$jammasuk		= $this->input->post('timein').":00";		
//		$jamkeluar		= $this->input->post('jamkeluar').":00";
		$tarra			= $this->input->post('tarra');
		$harga			= $this->input->post('harga');
		$sortasiptg		= $this->input->post('sortasiptg');
		$sortasiberat	= $this->input->post('sortasikg');
//		$netto			= $this->input->post('nettokg');
		$beratbayar		= $this->input->post('nettobayarkg');		
		$beratmasuk		= $this->input->post('beratkg');
		$userid			= $this->session->userdata('UserID');		
		$kudmemberid	= $this->input->post('kudmemberid');
		$area			= $this->input->post('area');
		$blok			= $this->input->post('areablok');
		$tandan			= $this->input->post('jumltandan');
		
		
		if($submit=='SIMPAN') {			
			$this->db->trans_start();//-----------------------------------------------------START TRANSAKSI
			
			$notiket	= $this->_getNewDocketID($tgldocket);
			
			$datadocket	= array(
							'DocketID'		=> $notiket,
							'DocketDate'	=> $tgldocket,
							'TruckID'		=> $nokendaraan,
							'ContractorID'	=> $contractorid,
							'ProductID'		=> '2',
//							'TripStartTime'	=> $tripstart,
							'TimeIn'		=> $jammasuk,
//							'TimeOut'		=> $jamkeluar,
							'SPBID'			=> $spbno,
				'KUDMemberID'	=> $kudmemberid,
							'SPBDate'		=> $spbdate,
							'LFWgt'			=> 0,
							'GrossWgt'		=> $beratmasuk,
							'TareWgt'		=> $tarra,
							'SortasiWgt'	=> $sortasiberat,
							'SortasiPtg'	=> $sortasiptg,
							'DriverName'	=> $driver,
							'PriceAmt'		=> $harga,
							'InOutFlg'		=> 'o',
							'InWBID'		=> $userid,
							'OutWBID'		=> $userid,
							'ManualFlg'		=> 1,
							'UserID'		=> $userid
					);
			$this->db->insert('ffbwbtrn', $datadocket);

			$datatbs	= array(
							'DocketID'		=> $notiket,								
							'AreaID'		=> $area,
							'BlockID'		=> $blok,
							'FFBCnt'		=> $tandan,
							'FFBWeight'		=> $beratbayar,
							'FFBGrossWgt'	=> $beratmasuk,
							'FFBTareWgt'	=> $tarra,
							'FFBSortasiWgt'	=> $sortasiberat,
							'LFWeight'		=> 0
						);
			$this->db->insert('ffbwbdtltrn', $datatbs);

			$updatedocketno	= "UPDATE companymst SET LastDONo='$notiket'";
			$this->db->query($updatedocketno);

			$this->db->trans_complete();//----------------------------------------------------END TRANSAKSI
		}
		redirect('trd/tbsmasuk', 'refresh');
	} 
	
	function cetak($docketid) {
		$this->load->helper('text');
		$data['nl']="\n";
		$pksdata				= $this->_getPKSData();
        $data['namaperusahaan'] = $pksdata['CompanyName'];
		$data['namapabrik']     = $pksdata['MillName'];
        $data['kepalatimbangan']= $pksdata['WBChiefName'];
		
        $data['ffbwbtrn']       = $this->_getTiketTBS($docketid);	
		$data['ffbwbdtltrn']    = $this->_getFfbDtlTrnDB($docketid);
		//------------------------------------------------------------------------------
    	$tikettbs 	= $this->load->view('trading/tbsmasuk/tbsmasuk_tiket',$data,TRUE);
    	$filename			= 'tiket_tbs';
    	$ext				= 'ctk';
    	header('Content-Disposition: attachment; filename="' . $filename . '.' . $ext . '"');
      	header("Content-Transfer-Encoding: binary");
      	header('Expires: 0');
   	 	header('Pragma: no-cache');
     	print $tikettbs;
		
	}
	
	function _view_data($num, $offset, $key, $category) {
	 	if($offset !='')
            $offset = $offset.',';
        
        $sql = "SELECT f.DocketID, f.DocketDate, a.AreaName, f.TruckID, c.ContractorName, fd.FFBWeight
				FROM ffbwbtrn f, areamst a, ffbwbdtltrn fd, contractormst c
				WHERE f.DocketID=fd.DocketID AND fd.AreaID=a.AreaID AND c.ContractorID=f.ContractorID AND f.ProductID='2' AND f.IsSell=0";
		if($key!=='')
			$sql .=" AND $category LIKE '%$key%'";
		$sql .=" ORDER BY f.DocketID DESC, f.DocketDate DESC, a.AreaName LIMIT $offset $num";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
	}
	
	function _getNumRowsItem($key,$category) {
		$sql = "SELECT f.DocketID
				FROM ffbwbtrn f, areamst a, ffbwbdtltrn fd, contractormst c
				WHERE f.DocketID=fd.DocketID AND fd.AreaID=a.AreaID AND c.ContractorID=f.ContractorID AND f.ProductID='2' AND f.IsSell=0";
		if($key !='')
			$sql .=" AND $category LIKE '%$key%'";
        $query = $this->db->query($sql);
        $num = $query->num_rows();
        return $num;
	}
	
	function _getPKSData(){
		$sql = "SELECT CompanyName, MillName, WBChiefName FROM companymst";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return  $result[0];
    }
	
	function _getFfbDtlTrnDB($docketid){
		$sql = "SELECT f.AreaID, a.AreaName, f.BlockID, f.FFBCnt, f.FFBGrossWgt, f.FFBTareWgt FROM ffbwbdtltrn AS f, areamst AS a
				WHERE f.AreaID=a.AreaID AND DocketID='$docketid'";
		$query = $this->db->query($sql); 
		$result = $query->result_array();
		return $result; 		
	}
	
	function _getTiketTBS($docketid){
		$sql = "SELECT c.ContractorName, f.SPBID, f.SPBDate, f.DocketDate, f.DOID, f.DocketID, (f.GrossWgt - f.TareWgt) AS netto, f.PriceAmt, 
						(f.GrossWgt - (f.TareWgt + f.SortasiWgt) ) AS nettobayar, f.ContractorID, f.KUDMemberID, k.KUDMemberName,
		               f.TruckID, p.ProductName, f.TripStartTime, f.LFWgt, f.TimeIn, f.TimeOut, f.GrossWgt, f.TareWgt, f.DriverName, f.SortasiPtg, f.SortasiWgt
				FROM contractormst c, productmst p, ffbwbtrn f, kudmembermst k
				WHERE DocketID='$docketid' AND f.ProductID=p.ProductID AND f.ContractorID=c.ContractorID AND f.KUDMemberID=k.KUDMemberID";
		$query = $this->db->query($sql); 
		$result = $query->result_array();
		return $result;
	}
	
	function _getWBDate() {
		$sql = "SELECT WBDate FROM customst";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return  $result[0]['WBDate'];
	}
	
	function _getAreaList() {
		$sql	= "SELECT  AreaID, AreaName FROM areamst ORDER BY AreaName";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result;
	}
	
	function _getContractorList() {
		$sql	= "SELECT  ContractorID, ContractorName FROM contractormst ORDER BY ContractorID";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result;
	}
	
	function _getNewDocketID($tgldocket) {
		list($thn, $bln, $hr) = explode("-",$tgldocket);
		$thnbulan	= ($thn - 2000) . $bln;
		$sql	= "SELECT DocketID FROM ffbwbtrn WHERE DocketID LIKE '$thnbulan%' ORDER BY DocketID DESC LIMIT 1";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		$lastno	= isset($result[0]['DocketID'])?$result[0]['DocketID']:$thnbulan.'0000';
		$currentno	= $lastno + 1;
		return $currentno;
	}
	
	function _getKUDMemberList() {
		$sql	= "SELECT  KUDMemberID, KUDMemberName FROM kudmembermst ORDER BY KUDMemberName";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result;
	}
		
	function test() {		
		$tgldocket	='2015-09-01';
		$x	= $this->_getNewDocketID($tgldocket);
		print_array($x);
	}
} 