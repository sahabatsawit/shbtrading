<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class LapTBSMasuk extends Authcontroller {
	
    var $isusermodify;
	
	function __construct() {
		parent::__construct();
        define("MENU_ID", "111");
        $userid = $this->session->userdata('UserID'); 
        $this->redirectNoAuthRead($userid,MENU_ID);
        $this->isusermodify = $this->isUserAuthModify($userid,MENU_ID);
	}
	  
	function index() {
	 	$data['kudmembermst'] = $this->_kudmembermst();
		$this->load->view('trading/laporan/laptbsmasuk_index', $data);
	}
	
	function _kudmembermst() {
		$sql	= "SELECT  KUDMemberID, KUDMemberName FROM kudmembermst ORDER BY KUDMemberName";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result;
	}
	
	function cetak() {
	    $from = $this->input->post('from');
	    $until = $this->input->post('until');
	    $KUDMemeberID = $this->input->post('KUDMemeberID');
		$qry = "SELECT 
						k.KUDMemberName,
						a.AreaName,
						h.DocketDate,
						h.DocketID,
						TIME_FORMAT(h.TimeIn, '%H:%i') AS TimeIn,
						h.TruckID,
						(d.FFBGrossWgt - d.FFBTareWgt) AS Netto,
						d.FFBSortasiWgt AS Sortasi,
						h.PriceAmt,
						(d.FFBGrossWgt-d.FFBTareWgt-d.FFBSortasiWgt) AS BayarKg,
						(d.FFBGrossWgt-d.FFBTareWgt-d.FFBSortasiWgt) * h.PriceAmt AS BayarRp
					FROM ffbwbtrn h, ffbwbdtltrn d, areamst a, kudmembermst k
					WHERE h.DocketID = d.DocketID AND d.AreaID = a.AreaID AND h.DocketDate BETWEEN '$from' AND '$until' 
						AND h.ProductID = '2' AND h.IsSell=0 AND h.KUDMemberID=k.KUDMemberID ";
		if($KUDMemeberID != 'all'){
			$qry .=  "AND k.KUDMemberID='$KUDMemeberID' ";
		} 
		$qry .= "HAVING Netto <> 0
				ORDER BY k.KUDMemberName, a.AreaName, h.DocketDate,h.DocketID";
	    
		if($this->input->post('submit') == 'PDF') {
			$this->load->library('PDF_MYSQL_Table');
			$pdf = new PDF_MySQL_Table();
			$pdf->Open();
			$pdf->PageOrientation('L');
			$pdf->AddPage();			
			$pdf->SetTitle("TBS Masuk");
			$pdf->AddCriteria("Tanggal ", 10, $from. " s/d " . $until , 50);
					
			$aSubTotal = array(array('KUDMemberName'=>0,'AreaName'=>0,'DocketDate'=>0,'DocketID'=>0, 'TimeIn'=>0, 'TruckID'=>0,
				'PriceAmt'=>0,'Netto'=>0,'Sortasi'=>0,'BayarKg'=>0,'BayarRp'=>0));
			
			$aGrandTotal = array('KUDMemberName'=>0,'AreaName'=>0,'DocketDate'=>0,'DocketID'=>0, 'TimeIn'=>0, 'TruckID'=>0,
				'PriceAmt'=>0,'Netto'=>0,'Sortasi'=>0,'BayarKg'=>0,'BayarRp'=>0);
			$pdf->SetGrandTotal($aGrandTotal);
			
			$pdf->SetSubTotalBy($aSubTotal);
			$pdf->AddCol("KUDMemberName",			30, "Anggota KUD",			"L", 1, -1);
			$pdf->AddCol("AreaName",				36, "Area",					"L", 0, -1);
			$pdf->AddCol("DocketDate",				16, "Tgl",					"L", 0, -1);
			$pdf->AddCol("DocketID",				15, "Ticket",				"L", 0, -1);
			$pdf->AddCol("TimeIn",					10, "In",					"L", 0, -1);
			$pdf->AddCol("TruckID",					18, "Truck",				"L", 0, -1);
			$pdf->AddCol("Netto",					12, "Netto",				"R", 0, 0, 'EN', 0);
			$pdf->AddCol("Sortasi",					10, "Sort",					"R", 0, 0, 'EN', 0);			
			$pdf->AddCol("PriceAmt",				13, "Harga",				"R", 0, 0, 'EN', 0);
			$pdf->AddCol("BayarKg",					20, "Bayar(Kg)",				"R", 0, 0, 'EN', 0);
			$pdf->AddCol("BayarRp",					20, "Bayar(Rp)",				"R", 0, 0, 'EN', 0);
			
			$pdf->Table($qry);
			$pdf->output();
		} else {
			$this->load->library('ExportToCSV');
			$export = new ExportToCSV();
			$export->SetQuery($qry);
			$export->Export();
		}	
	}
}