<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);

class TbsKeluar extends Authcontroller {
    /**
	 *
	 */
    var $isusermodify;
    
   	function __construct() {
        parent::__construct();
        define("MENU_ID", "102");
        $userid = $this->session->userdata('UserID'); 
        $this->redirectNoAuthRead($userid,MENU_ID);
        $this->isusermodify = $this->isUserAuthModify($userid,MENU_ID);
    }
	
	function index() {
		$this->load->library('pagination');
		$this->load->helper('text');
		$urisegment	= 6;
		$keyword	= $this->uri->segment($urisegment -2);
		$keywordurl = $keyword;
		$keywordurl2 = $this->uri->segment($urisegment -1);

		if(($keyword=='') || ($keyword=='nokeyword')){
				$keyword	= '';
				$keywordurl = 'nokeyword';
				$keywordurl2 = 'nokeyword';
		}
        if($this->input->post('submit')=='Cari'){
			$keyword    = $this->input->post('optionValue');
			$keywordurl2 = $this->input->post('option');
			if($keyword ==''){
				$keywordurl = 'nokeyword';
				$keywordurl2 = 'nokeyword';
			}
			else{
				$keywordurl = $keyword;
			}
		}
		$dataperpage				= 11;
		$config['per_page']         = $dataperpage;
		$config['base_url']         = site_url()."/trd/tbskeluar/index/$keywordurl/$keywordurl2/";
		$config['uri_segment']      = $urisegment;
        $config['total_rows']       = $this->_getNumRowsItem($keyword, $keywordurl2);

        $this->pagination->initialize($config);
		$fromurisegment				= $this->uri->segment($urisegment);
		$data['view_data']			= $this->_view_data($dataperpage, $fromurisegment, $keyword, $keywordurl2);
		$data['wbdate']				= $this->_getWBDate();
		$this->load->view('trading/tbskeluar/tbskeluar_index', $data);
	}
	
	function edit($docketid) {
		$this->load->helper('text');
		$data['contractormst']	= $this->_getContractorList();
		$data['customermst']	= $this->_getCustomerList();
		$data['kudmembermst']	= $this->_getKUDMemberList();
		$data['datatbs_arr']	= $this->_getTiketTBS($docketid);
		$data['urlsegment']		= $this->uri->uri_string();
		$this->load->view('trading/tbskeluar/tbskeluar_edit', $data);
	}
	
	function editproc($docketid) {
		$submit			= $this->input->post('submit');
		$nokendaraan	= $this->input->post('noregkendaraan');
		$driver			= $this->input->post('namadriver');
		$tgldocket		= $this->input->post('tgltiket');
		$contractorid	= $this->input->post('angkutan');
		$customerid		= $this->input->post('customerid');
		$kudmemberid	= $this->input->post('kudmemberid');
		$spbdate		= $this->input->post('tglspb');
		$spbno			= $this->input->post('nospb');		
		$jamkeluar		= $this->input->post('timeout').":00";
		$tarra			= $this->input->post('tarra');
		$harga			= $this->input->post('harga');
		$netto			= $this->input->post('nettokg');
		$nettoditerima	= $this->input->post('nettoditerimakg');
		$beratmasuk		= $this->input->post('beratkg');
		$userid			= $this->session->userdata('UserID');
		
		
		if($submit=='SIMPAN') {			
			$this->db->trans_start();//-----------------------------------------------------START TRANSAKSI
			
			$datadocket	= array(
							'DocketDate'	=> $tgldocket,
							'TruckID'		=> $nokendaraan,
							'ContractorID'	=> $contractorid,
							'CustomerID'	=> $customerid,
							'KUDMemberID'	=> $kudmemberid,
							'TimeOut'		=> $jamkeluar,
							'SPBID'			=> $spbno,
							'SPBDate'		=> $spbdate,
							'GrossWgt'		=> $beratmasuk,
							'TareWgt'		=> $tarra,
							'DriverName'	=> $driver,
							'PriceAmt'		=> $harga,
							'ReceivedWgt'	=> $nettoditerima,
							'UserID'		=> $userid
					);
			$this->db->update('ffbwbtrn', $datadocket, array('DocketID'	=> $docketid));
			
			$this->db->trans_complete();//----------------------------------------------------END TRANSAKSI
		}
		
		// back to page asal	
		$urlstring	= $this->input->post('urlsegment');
		$urlarr = explode("/", $urlstring);
		$url	= '';
		$i	= 0;
		foreach ($urlarr as $uri) {
			if($i>5)
				$url	.= '/'.$uri;
			$i++;
		}
		redirect('trd/tbskeluar'.$url);
	}
	
	function lihat($docketid) {
		$this->load->helper('text');
		$data['contractormst']	= $this->_getContractorList();
		$data['customermst']	= $this->_getCustomerList();
		$data['kudmembermst']	= $this->_getKUDMemberList();
		$data['datatbs_arr']	= $this->_getTiketTBS($docketid);
		$data['urlsegment']		= $this->uri->uri_string();
		$this->load->view('trading/tbskeluar/tbskeluar_lihat', $data);
	}
	
	function input() {
		$data['wbdate']			= $this->_getWBDate();
		$data['areamst']		= $this->_getAreaList();
		$data['contractormst']	= $this->_getContractorList();
		$data['customermst']	= $this->_getCustomerList();
		$data['kudmembermst']	= $this->_getKUDMemberList();
		$this->load->view('trading/tbskeluar/tbskeluar_input', $data);
	}
	
	function getDataTruck_ajx($truckid) {
		$truckid	= urldecode($truckid);
		
		$sql = "SELECT ContractorID, DriverName, LicenseNumber, TruckTare, TruckID AS data "
			. "FROM truckmst "
			. "WHERE ProductID=2 AND TruckID LIKE '%$truckid%' LIMIT 40";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		echo json_encode($row);
    }
	
	function getTotalAnggotaByYear_ajx() {
		$tgltiket	= $this->input->post('tgltiket');
		$anggota	= $this->input->post('anggota');
		
		list($tahun, $bulan, $tanggal)	= explode('-',$tgltiket);
		
		$sql	= "SELECT SUM(receivedwgt * PriceAmt) AS TotalVal "
			. "FROM ffbwbtrn "
			. "WHERE ProductID='2' AND IsSell=1 AND KUDMemberID=$anggota AND YEAR(DocketDate)=$tahun";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		$totalval	= isset($result[0]['TotalVal'])?$result[0]['TotalVal']:0;
		
		$sql2	= "SELECT SUM( (GrossWgt - TareWgt) * IF(IsSell=0,1,-1) ) AS TotalStok "
			. "FROM ffbwbtrn "
			. "WHERE ProductID='2' AND KUDMemberID=$anggota";
		$query2	= $this->db->query($sql2);
		$result2	= $query2->result_array();
		$totalstok	= isset($result2[0]['TotalStok'])?$result2[0]['TotalStok']:0;
		
		$retval	= array('TotalVal'=>$totalval, 'TotalStok'=>$totalstok);
		echo 'datajson = '.json_encode($retval);
	}
	
	function inputproc() {
		$submit			= $this->input->post('submit');
		$nokendaraan	= $this->input->post('noregkendaraan');
		$driver			= $this->input->post('namadriver');
		$tgldocket		= $this->input->post('tgltiket');
		$contractorid	= $this->input->post('angkutan');
		$customerid		= $this->input->post('customerid');
		$kudmemberid	= $this->input->post('kudmemberid');
		$spbdate		= $this->input->post('tglspb');
		$spbno			= $this->input->post('nospb');		
		$jamkeluar		= $this->input->post('timeout').":00";
		$tarra			= $this->input->post('tarra');
		$harga			= $this->input->post('harga');
		$netto			= $this->input->post('nettokg');
		$nettoditerima	= $this->input->post('nettoditerimakg');		
		$beratmasuk		= $this->input->post('beratkg');
		$userid			= $this->session->userdata('UserID');
		
		if($submit=='SIMPAN') {
			$this->db->trans_start();//-----------------------------------------------------START TRANSAKSI
			
			$notiket	= $this->_getNewDocketID();
			
			$datadocket	= array(
							'DocketID'		=> $notiket,
							'DocketDate'	=> $tgldocket,
							'TruckID'		=> $nokendaraan,
							'ContractorID'	=> $contractorid,
							'ProductID'		=> '2',
							'CustomerID'	=> $customerid,
							'KUDMemberID'	=> $kudmemberid,
							'TimeOut'		=> $jamkeluar,
							'SPBID'			=> $spbno,
							'SPBDate'		=> $spbdate,
							'LFWgt'			=> 0,
							'GrossWgt'		=> $beratmasuk,
							'TareWgt'		=> $tarra,
							'DriverName'	=> $driver,
							'PriceAmt'		=> $harga,
							'ReceivedWgt'	=> $nettoditerima,
							'InOutFlg'		=> 'o',
							'InWBID'		=> $userid,
							'OutWBID'		=> $userid,
							'IsSell'		=> 1,
							'ManualFlg'		=> 1,
							'UserID'		=> $userid
					);
			$this->db->insert('ffbwbtrn', $datadocket);

			$updatedocketno	= "UPDATE companymst SET LastDONo='$notiket'";
			$this->db->query($updatedocketno);

			$this->db->trans_complete();//----------------------------------------------------END TRANSAKSI
		}
		redirect('trd/tbskeluar', 'refresh');
	} 
	
	function cetak($docketid) {
		$this->load->helper('text');
		$data['nl']="\n";
		$pksdata				= $this->_getPKSData();
        $data['namaperusahaan'] = $pksdata['CompanyName'];
		$data['namapabrik']     = $pksdata['MillName'];
        $data['kepalatimbangan']= $pksdata['WBChiefName'];
		
        $data['ffbwbtrn']       = $this->_getTiketTBS($docketid);
		//------------------------------------------------------------------------------
    	$tikettbs 	= $this->load->view('trading/tbskeluar/tbskeluar_tiket',$data,TRUE);
    	$filename			= 'tiketkeuar_tbs';
    	$ext				= 'ctk';
    	header('Content-Disposition: attachment; filename="' . $filename . '.' . $ext . '"');
      	header("Content-Transfer-Encoding: binary");
      	header('Expires: 0');
   	 	header('Pragma: no-cache');
     	print $tikettbs;
		
	}
	
	function _view_data($num, $offset, $key, $category) {
	 	if($offset !='')
            $offset = $offset.',';
        
        $sql = "SELECT f.DocketID, f.DocketDate, f.TruckID, c.ContractorName, cs.CustomerName, (f.GrossWgt - f.TareWgt) AS FFBWeight
				FROM ffbwbtrn f, contractormst c, customermst cs, kudmembermst k
				WHERE c.ContractorID=f.ContractorID AND f.CustomerID=cs.CustomerID AND k.KUDMemberID=f.KUDMemberID AND f.ProductID='2' AND f.IsSell=1";
		if($key!=='')
			$sql .=" AND $category LIKE '%$key%'";
		$sql .=" ORDER BY f.DocketID DESC, f.DocketDate DESC, cs.CustomerName LIMIT $offset $num";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
	}
	
	function _getNumRowsItem($key,$category) {
		$sql = "SELECT f.DocketID
				FROM ffbwbtrn f, contractormst c, customermst cs, kudmembermst k
				WHERE c.ContractorID=f.ContractorID AND f.CustomerID=cs.CustomerID AND k.KUDMemberID=f.KUDMemberID AND f.ProductID='2' AND f.IsSell=1";
		if($key !='')
			$sql .=" AND $category LIKE '%$key%'";
        $query = $this->db->query($sql);
        $num = $query->num_rows();
        return $num;
	}
	
	function _getPKSData(){
		$sql = "SELECT CompanyName, MillName, WBChiefName FROM companymst";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return  $result[0];
    }
	
	function _getFfbDtlTrnDB($docketid){
		$sql = "SELECT f.AreaID, a.AreaName, f.BlockID, f.FFBCnt, f.FFBGrossWgt, f.FFBTareWgt FROM ffbwbdtltrn AS f, areamst AS a
				WHERE f.AreaID=a.AreaID AND DocketID='$docketid'";
		$query = $this->db->query($sql); 
		$result = $query->result_array();
		return $result; 		
	}
	
	function _getTiketTBS($docketid){
		$sql = "SELECT c.ContractorName, f.SPBID, f.SPBDate, f.DocketDate, f.DOID, f.DocketID, (f.GrossWgt - f.TareWgt) AS netto, f.PriceAmt, f.KUDMemberID, k.KUDMemberName,
						(f.GrossWgt - (f.TareWgt + f.SortasiWgt) ) AS nettobayar, f.ContractorID, f.ReceivedWgt, f.CustomerID, cs.CustomerName,
		               f.TruckID, p.ProductName, f.TripStartTime, f.LFWgt, f.TimeIn, f.TimeOut, f.GrossWgt, f.TareWgt, f.DriverName, f.SortasiPtg, f.SortasiWgt
				FROM contractormst c, productmst p, ffbwbtrn f, kudmembermst k, customermst cs
				WHERE DocketID='$docketid' AND f.ProductID=p.ProductID AND f.ContractorID=c.ContractorID AND cs.CustomerID=f.CustomerID AND k.KUDMemberID=f.KUDMemberID";
		$query = $this->db->query($sql); 
		$result = $query->result_array();
		return $result;
	}
	
	function _getWBDate() {
		$sql = "SELECT WBDate FROM customst";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return  $result[0]['WBDate'];
	}
	
	function _getAreaList() {
		$sql	= "SELECT  AreaID, AreaName FROM areamst ORDER BY AreaName";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result;
	}
	
	function _getContractorList() {
		$sql	= "SELECT  ContractorID, ContractorName FROM contractormst ORDER BY ContractorName";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result;
	}
	
	function _getKUDMemberList() {
		$sql	= "SELECT  KUDMemberID, KUDMemberName FROM kudmembermst ORDER BY KUDMemberName";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result;
	}
	
	function _getCustomerList() {
		$sql	= "SELECT  CustomerID, CustomerName FROM customermst ORDER BY CustomerName";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result;
	}
	
	function _getNewDocketID() {
		$sql	= "SELECT LastDONo FROM companymst LIMIT 1";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		$lastno	= $result[0]['LastDONo'];
		$currentno	= $lastno + 1;
		return $currentno;
	}
		
	function test() {		
		print_array();
	}
} 