<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class LapTBSHarian extends Authcontroller {
	
    var $isusermodify;
	
	function __construct() {
		parent::__construct();
        define("MENU_ID", "114");
        $userid = $this->session->userdata('UserID'); 
        $this->redirectNoAuthRead($userid,MENU_ID);
        $this->isusermodify = $this->isUserAuthModify($userid,MENU_ID);
	}
	  
	function index() {
		$this->load->view('trading/laporan/laptbsharian_index');
	}
	
	function process() {
		$tgl			= $this->input->post('from');
		$is_submitted	= $this->input->post('submit');
		if($is_submitted) {
			$this->_xls($tgl);
			
			$filename = 'laptbsharian.xls';			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');
			
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
			$objWriter->save('php://output');
		}
	}
	
	function _xls($tgl) {
		// number format, with thousands separator and no decimal points.
		$numberFormat = '#,#0;[Red]-#,#0';
		$this->load->library('excel');
		list($thn,$bln,$day)	= explode('-',$tgl);
		
		$datasaldo	= $this->_getSaldoAwal($tgl);
		$stokawal		= $datasaldo['Awal'];
		$dibayar		= $datasaldo['MasukDibayar'];
		$beratkotor		= $datasaldo['MasukGross'];
		$sortasi		= $beratkotor - $dibayar;
				
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle($tgl);
		$this->excel->getActiveSheet()->setCellValue('A1', 'LAPORAN TBS HARIAN');
		$this->excel->getActiveSheet()->setCellValue('A3', 'Tanggal : '.$day.' '. indMonthName($bln). ' '.$thn);
		$this->excel->getActiveSheet()->setCellValue('A4', 'Stok Awal : '.$stokawal.' Kg');
		$this->excel->getActiveSheet()->setCellValue('A5', 'Pembelian : '.$dibayar.' Kg');
		$this->excel->getActiveSheet()->setCellValue('A6', 'Sortasi : '.$sortasi.' Kg');
		$this->excel->getActiveSheet()->getStyle('A3:A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); 
		$this->excel->getActiveSheet()->setCellValue('A8', 'Penjualan');
		$this->excel->getActiveSheet()->setCellValue('A9', 'Pelanggan');
		$this->excel->getActiveSheet()->setCellValue('B9', 'Keluar (Kg)');
		$this->excel->getActiveSheet()->setCellValue('C9', 'Diterima (Kg)');
		$this->excel->getActiveSheet()->setCellValue('D9', 'Selisih (Kg)');
		
		$totkeluar	= 0;
		$totditerima= 0;
		$row	= 10;
		$datamutations	= $this->_getMutationByDate($tgl);
		foreach ($datamutations as $datamutation) {
			$anggota	= $datamutation['KUDMemberName'];
			$keluar		= $datamutation['FfbOutWgt'];
			$diterima	= $datamutation['ReceivedWgt'];
			$selisih	= $diterima - $keluar;
			
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $anggota);
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $keluar);
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $diterima);
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $selisih);
			
			$totkeluar		+= $keluar;
			$totditerima	+= $diterima;
			
			$row++;
		}
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, 'Total');
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $totkeluar);
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $totditerima);
		$row++;
		$row++;
		$totakhir	= $stokawal + $beratkotor - $totkeluar;
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, 'Total Akhir');
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $totakhir);
//		$this->excel->getActiveSheet()->mergeCells('A4:L4');
		
		
	}
	
	function _getMutationByDate($tgl) {
		$sql	= "SELECT f.KUDMemberID, k.KUDMemberName, f.FfbOutWgt, f.ReceivedWgt
					FROM ffbmutationtrn f, kudmembermst k
					WHERE f.FfbDate = '$tgl' AND k.KUDMemberID=f.KUDMemberID ORDER BY k.KUDMemberName";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();	
		return $row;
	}
		
	function _getSaldoAwal($tgl) {
		$datakosong	= array('Awal'=>0,'MasukDibayar'=>0,'MasukGross'=>0);
		$sql	= "SELECT SUM(StartWgt) AS Awal, SUM(FfbInPayWgt) AS MasukDibayar, SUM(FfbInWgt) AS MasukGross 
					FROM ffbmutationtrn
					WHERE FfbDate = '$tgl'";
		$qry    = $this->db->query($sql);
		$row    = $qry->result_array();		
		$retval	= isset($row[0])?$row[0]:$datakosong;
		return $retval;
	}
	
	function get_col_letter($c){
		 $c = intval($c);

		if ($c <= 0) return '';

		$letter = '';
		while($c != 0){
			$p = ($c - 1) % 26;
			$c = intval(($c - $p) / 26);
			$letter = chr(65 + $p) . $letter;
		}
		return $letter;
	}
	
	function test() {
		$data	= $this->get_col_letter(3);
		print_r($data);
	}
}
