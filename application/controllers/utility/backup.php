<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Backup extends Authcontroller {
	
    var $isusermodify;
	
	function __construct(){
        parent::__construct();
        define("MENU_ID", "115");
        $userid = $this->session->userdata('UserID'); 
        $this->redirectNoAuthRead($userid,MENU_ID);
        $this->isusermodify = $this->isUserAuthModify($userid,MENU_ID);
	}
	
	function index() {
		$this->load->view('utility/backup/backup_index');
	}
	
	function process() {
		$submit	= $this->input->post('submit');
		if($submit) {
			$this->load->dbutil();
			//----------------------------------------------------------------------------------------------------
			$tablestobakup	= array(
								'areamst', 'blockmst', 'citymst', 'coamst','companymst', 'contractormst', 'customermst',
								'customst','ffbmutationtrn','ffbwbdtltrn', 'ffbwbtrn', 'kudmembermst', 'menumst', 'productmst',
								 'truckmst', 'usergroupdtlmst', 'usergroupmst', 'usermst'
							);
			$prefs			= array(
								'tables'	=> $tablestobakup,
								'ignore'	=> array(),
								'format'	=> 'txt',
								'filename'	=> 'shbtrading.sql',
								'add_drop'	=> TRUE, 
								'add_insert'=> TRUE,
								'newline'	=> "\n"
							);

			$backup	= $this->dbutil->backup($prefs);
			$this->load->helper('download');
			$totime	= date("Ymd_Hi");
			force_download('shbtrading_backup'.$totime.'.sql', $backup);
		}
	}
}
