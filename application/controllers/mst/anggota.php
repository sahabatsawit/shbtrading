<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);

class Anggota extends Authcontroller {
    /**
	 *
	 */
    var $isusermodify;
    
   	function __construct() {
        parent::__construct();
        define("MENU_ID", "108");
        $userid = $this->session->userdata('UserID'); 
        $this->redirectNoAuthRead($userid,MENU_ID);
        $this->isusermodify = $this->isUserAuthModify($userid,MENU_ID);
    }
	
	function index() {
		$this->load->library('pagination');
		$this->load->helper('text');
		$urisegment	= 6;
		$keyword	= $this->uri->segment($urisegment -2);
		$keywordurl = $keyword;
		$keywordurl2 = $this->uri->segment($urisegment -1);

		if(($keyword=='') || ($keyword=='nokeyword')){
				$keyword	= '';
				$keywordurl = 'nokeyword';
				$keywordurl2 = 'nokeyword';
		}
        if($this->input->post('submit')=='Cari'){
			$keyword    = $this->input->post('optionValue');
			$keywordurl2 = $this->input->post('option');
			if($keyword ==''){
				$keywordurl = 'nokeyword';
				$keywordurl2 = 'nokeyword';
			}
			else{
				$keywordurl = $keyword;
			}
		}
		$dataperpage				= 11;
		$config['per_page']         = $dataperpage;
		$config['base_url']         = site_url()."/mst/anggota/index/$keywordurl/$keywordurl2/";
		$config['uri_segment']      = $urisegment;
        $config['total_rows']       = $this->_getNumRowsItem($keyword, $keywordurl2);

        $this->pagination->initialize($config);
		$fromurisegment				= $this->uri->segment($urisegment);
		$data['view_data']			= $this->_view_data($dataperpage, $fromurisegment, $keyword, $keywordurl2);
		$this->load->view('master/anggota/anggota_index', $data);
	}
	
	function _view_data($num, $offset, $key, $category) {
	 	if($offset !='')
            $offset = $offset.',';
        
        $sql = "SELECT KUDMemberID, KUDMemberName, Address, Telp, ContactName FROM kudmembermst ";
		if($key!=='')
			$sql .=" WHERE $category LIKE '%$key%'";
		$sql .=" ORDER BY KUDMemberID DESC LIMIT $offset $num";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
	}
	
	function _getNumRowsItem($key,$category) {
		$sql = "SELECT KUDMemberID FROM kudmembermst";
		if($key !='')
			$sql .=" WHERE $category LIKE '%$key%'";
        $query = $this->db->query($sql);
        $num = $query->num_rows();
        return $num;
	}
	
	function input() {
		$this->load->helper('text');
		$id					= null;
		$data['data']		= $this->_getData($id);
		$data['urlsegment']	= $this->uri->uri_string();
		$this->load->view('master/anggota/anggota_edit', $data);
	}
	
	function _getData($id){
		$datakosong	= array(
			'KUDMemberID'	=> null,
			'KUDMemberName'	=> '',
			'Address'		=> '',
			'Telp'			=> '',
			'ContactName'	=> ''
		);
		
		$sql = "SELECT KUDMemberID, KUDMemberName, Address, Telp, ContactName FROM kudmembermst WHERE KUDMemberID='$id'";
		$query = $this->db->query($sql); 
		$result = $query->result_array();
		$retval	= isset($result[0])?$result[0]:$datakosong;
		
		return $retval;
	}
	
	function inputeditproc($id=null) {
		if(is_null($id)) {
			$this->_inputproc();
		} else {
			$this->_editproc($id);
		}
	}
	
	function _inputproc() {
		$submit			= $this->input->post('submit');
		$namaanggota	= $this->input->post('kudmembername');
		$alamat			= $this->input->post('address');
		$kontak			= $this->input->post('contactname');
		$notelp			= $this->input->post('telp');
		$userid			= $this->session->userdata('UserID');		
				
		if($submit=='SIMPAN') {			
			$this->db->trans_start();//-----------------------------------------------------START TRANSAKSI
			
			$data	= array(
							'KUDMemberName'	=> $namaanggota,
							'Address'		=> $alamat,
							'ContactName'		=> $kontak,
							'Telp'			=> $notelp
					);
			$this->db->insert('kudmembermst', $data);
			
			$this->db->trans_complete();//----------------------------------------------------END TRANSAKSI
		}
		redirect('mst/anggota', 'refresh');
	} 	
	
	function edit($id) {
		$this->load->helper('text');
		$data['data']		= $this->_getData($id);
		$data['urlsegment']	= $this->uri->uri_string();
		$this->load->view('master/anggota/anggota_edit', $data);
	}
	
	function _editproc($id) {
		$submit			= $this->input->post('submit');
		$namaanggota	= $this->input->post('kudmembername');
		$alamat			= $this->input->post('address');
		$kontak			= $this->input->post('contactname');
		$notelp			= $this->input->post('telp');
		$userid			= $this->session->userdata('UserID');
		
		if($submit=='SIMPAN') {			
			$this->db->trans_start();//-----------------------------------------------------START TRANSAKSI
			
			$data	= array(
							'KUDMemberName'	=> $namaanggota,
							'Address'		=> $alamat,
							'ContactName'		=> $kontak,
							'Telp'			=> $notelp
					);
			$this->db->update('kudmembermst', $data, array('KUDMemberID'	=> $id));

			$this->db->trans_complete();//----------------------------------------------------END TRANSAKSI
		}
		
		// back to page asal	
		$urlstring	= $this->input->post('urlsegment');
		$urlarr = explode("/", $urlstring);
		$url	= '';
		$i	= 0;
		foreach ($urlarr as $uri) {
			if($i>5)
				$url	.= '/'.$uri;
			$i++;
		}
		redirect('mst/anggota'.$url, 'refresh');
	}	
			
	function test() {		
		print_array();
	}
} 