<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);

class Kendaraan extends Authcontroller {
    /**
	 *
	 */
    var $isusermodify;
    
   	function __construct() {
        parent::__construct();
        define("MENU_ID", "106");
        $userid = $this->session->userdata('UserID'); 
        $this->redirectNoAuthRead($userid,MENU_ID);
        $this->isusermodify = $this->isUserAuthModify($userid,MENU_ID);
    }
	
	function index() {
		$this->load->library('pagination');
		$this->load->helper('text');
		$urisegment	= 6;
		$keyword	= $this->uri->segment($urisegment -2);
		$keywordurl = $keyword;
		$keywordurl2 = $this->uri->segment($urisegment -1);

		if(($keyword=='') || ($keyword=='nokeyword')){
				$keyword	= '';
				$keywordurl = 'nokeyword';
				$keywordurl2 = 'nokeyword';
		}
        if($this->input->post('submit')=='Cari'){
			$keyword    = $this->input->post('optionValue');
			$keywordurl2 = $this->input->post('option');
			if($keyword ==''){
				$keywordurl = 'nokeyword';
				$keywordurl2 = 'nokeyword';
			}
			else{
				$keywordurl = $keyword;
			}
		}
		$dataperpage				= 11;
		$config['per_page']         = $dataperpage;
		$config['base_url']         = site_url()."/mst/kendaraan/index/$keywordurl/$keywordurl2/";
		$config['uri_segment']      = $urisegment;
        $config['total_rows']       = $this->_getNumRowsItem($keyword, $keywordurl2);

        $this->pagination->initialize($config);
		$fromurisegment				= $this->uri->segment($urisegment);
		$data['view_data']			= $this->_view_data($dataperpage, $fromurisegment, $keyword, $keywordurl2);
		$this->load->view('master/kendaraan/kendaraan_index', $data);
	}
	
	function _view_data($num, $offset, $key, $category) {
	 	if($offset !='')
            $offset = $offset.',';
        
        $sql = "SELECT t.TruckID, c.ContractorID, c.ContractorName, t.DriverName, t.LicenseNumber, t.TruckType
				FROM truckmst t, contractormst c WHERE t.ContractorID=c.ContractorID";
		if($key!=='')
			$sql .=" AND $category LIKE '%$key%'";
		$sql .=" ORDER BY t.TransDate DESC LIMIT $offset $num";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
	}
	
	function _getNumRowsItem($key,$category) {
		 $sql = "SELECT t.TruckID
				FROM truckmst t, contractormst c WHERE t.ContractorID=c.ContractorID";
		if($key!=='')
			$sql .=" AND $category LIKE '%$key%'";
        $query = $this->db->query($sql);
        $num = $query->num_rows();
        return $num;
	}
	
	function input() {
		$this->load->helper('text');
		$data['contractormst']	= $this->_getContractorList();
		$id					= null;
		$data['data']		= $this->_getData($id);
		$data['urlsegment']	= $this->uri->uri_string();
		$this->load->view('master/kendaraan/kendaraan_edit', $data);
	}
	
	function _getData($id){
		$datakosong	= array(
			'TruckID'		=> null,
			'ContractorID'	=> '',
			'TruckType'		=> '',
			'DriverName'	=> ''
		);
		
		$sql = "SELECT TruckID, ContractorID, TruckType, DriverName FROM truckmst WHERE TruckID='$id'";
		$query = $this->db->query($sql); 
		$result = $query->result_array();
		$retval	= isset($result[0])?$result[0]:$datakosong;
		
		return $retval;
	}
	
	function inputeditproc($id=null) {
		if(is_null($id)) {
			$this->_inputproc();
		} else {
			$this->_editproc($id);
		}
	}
	
	function _inputproc() {
		$submit			= $this->input->post('submit');
		$nopol			= $this->input->post('truckid');
		$angkutan		= $this->input->post('angkutan');
		$supir			= $this->input->post('drivername');
		$type			= $this->input->post('trucktype');
		$userid			= $this->session->userdata('UserID');		
				
		if($submit=='SIMPAN') {			
			$this->db->trans_start();//-----------------------------------------------------START TRANSAKSI
			
			$data	= array(
							'TruckID'		=> $nopol,
							'ContractorID'	=> $angkutan,
							'DriverName'	=> $supir,
							'TruckType'		=> $type,
							'UserID'		=> $userid
					);
			$this->db->insert('truckmst', $data);
			
			$this->db->trans_complete();//----------------------------------------------------END TRANSAKSI
		}
		redirect('mst/kendaraan', 'refresh');
	} 	
	
	function edit($id) {
		$this->load->helper('text');
		$data['contractormst']	= $this->_getContractorList();
		$data['data']		= $this->_getData($id);
		$data['urlsegment']	= $this->uri->uri_string();
		$this->load->view('master/kendaraan/kendaraan_edit', $data);
	}
	
	function _editproc($id) {
		$submit			= $this->input->post('submit');
		$nopol			= $this->input->post('truckid');
		$angkutan		= $this->input->post('angkutan');
		$supir			= $this->input->post('drivername');
		$type			= $this->input->post('trucktype');
		$userid			= $this->session->userdata('UserID');
		
		if($submit=='SIMPAN') {			
			$this->db->trans_start();//-----------------------------------------------------START TRANSAKSI
			
			$data	= array(
							'ContractorID'	=> $angkutan,
							'DriverName'	=> $supir,
							'TruckType'		=> $type,
							'UserID'		=> $userid
					);
			$this->db->update('truckmst', $data, array('TruckID'	=> $id));

			$this->db->trans_complete();//----------------------------------------------------END TRANSAKSI
		}
		
		// back to page asal	
		$urlstring	= $this->input->post('urlsegment');
		$urlarr = explode("/", $urlstring);
		$url	= '';
		$i	= 0;
		foreach ($urlarr as $uri) {
			if($i>5)
				$url	.= '/'.$uri;
			$i++;
		}
		redirect('mst/kendaraan'.$url, 'refresh');
	}	
	
	function _getContractorList() {
		$sql	= "SELECT  ContractorID, ContractorName FROM contractormst WHERE ActiveFlg=1 ORDER BY ContractorName";
		$query	= $this->db->query($sql);
		$result	= $query->result_array();
		return $result;
	}
	
	function test() {		
		print_array();
	}
} 