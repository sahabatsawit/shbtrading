<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);

class Area extends Authcontroller {
    /**
	 *
	 */
    var $isusermodify;
    
   	function __construct() {
        parent::__construct();
        define("MENU_ID", "103");
        $userid = $this->session->userdata('UserID'); 
        $this->redirectNoAuthRead($userid,MENU_ID);
        $this->isusermodify = $this->isUserAuthModify($userid,MENU_ID);
    }
	
	function index() {
		$this->load->library('pagination');
		$this->load->helper('text');
		$urisegment	= 6;
		$keyword	= $this->uri->segment($urisegment -2);
		$keywordurl = $keyword;
		$keywordurl2 = $this->uri->segment($urisegment -1);

		if(($keyword=='') || ($keyword=='nokeyword')){
				$keyword	= '';
				$keywordurl = 'nokeyword';
				$keywordurl2 = 'nokeyword';
		}
        if($this->input->post('submit')=='Cari'){
			$keyword    = $this->input->post('optionValue');
			$keywordurl2 = $this->input->post('option');
			if($keyword ==''){
				$keywordurl = 'nokeyword';
				$keywordurl2 = 'nokeyword';
			}
			else{
				$keywordurl = $keyword;
			}
		}
		$dataperpage				= 11;
		$config['per_page']         = $dataperpage;
		$config['base_url']         = site_url()."/mst/area/index/$keywordurl/$keywordurl2/";
		$config['uri_segment']      = $urisegment;
        $config['total_rows']       = $this->_getNumRowsItem($keyword, $keywordurl2);

        $this->pagination->initialize($config);
		$fromurisegment				= $this->uri->segment($urisegment);
		$data['view_data']			= $this->_view_data($dataperpage, $fromurisegment, $keyword, $keywordurl2);
		$this->load->view('master/area/area_index', $data);
	}
	
	function _view_data($num, $offset, $key, $category) {
	 	if($offset !='')
            $offset = $offset.',';
        
        $sql = "SELECT AreaID, AreaName, Address, ContactPerson, NPWP
				FROM areamst ";
		if($key!=='')
			$sql .=" WHERE $category LIKE '%$key%'";
		$sql .=" ORDER BY TransDate DESC LIMIT $offset $num";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
	}
	
	function _getNumRowsItem($key,$category) {
		$sql = "SELECT AreaID FROM areamst";
		if($key !='')
			$sql .=" WHERE $category LIKE '%$key%'";
        $query = $this->db->query($sql);
        $num = $query->num_rows();
        return $num;
	}
	
	function input() {
		$this->load->helper('text');
		$id					= null;
		$data['data']		= $this->_getData($id);
		$data['urlsegment']	= $this->uri->uri_string();
		$this->load->view('master/area/area_edit', $data);
	}
	
	function _getData($id){
		$datakosong	= array(
			'AreaID'			=> null,
			'AreaName'			=> '',
			'Address'			=> '',
			'ContactPerson'		=> ''
		);
		
		$sql = "SELECT AreaID, AreaName, Address, ContactPerson FROM areamst WHERE AreaID='$id'";
		$query = $this->db->query($sql); 
		$result = $query->result_array();
		$retval	= isset($result[0])?$result[0]:$datakosong;
		
		return $retval;
	}
	
	function inputeditproc($id=null) {
		if(is_null($id)) {
			$this->_inputproc();
		} else {
			$this->_editproc($id);
		}
	}
	
	function _inputproc() {
		$submit			= $this->input->post('submit');
		$namaarea		= $this->input->post('areaname');
		$alamat			= $this->input->post('address');
		$kontak			= $this->input->post('contactperson');
		$userid			= $this->session->userdata('UserID');		
				
		if($submit=='SIMPAN') {			
			$this->db->trans_start();//-----------------------------------------------------START TRANSAKSI
			
			$data	= array(
							'AreaName'		=> $namaarea,
							'Address'		=> $alamat,
							'ContactPerson'	=> $kontak,
							'UserID'		=> $userid
					);
			$this->db->insert('areamst', $data);
			
			$datablok	= array(
							'AreaID'		=> $this->_getLastInsertedID(),
							'BlockID'		=> 1,
					);
			$this->db->insert('blockmst', $datablok);
			$this->db->trans_complete();//----------------------------------------------------END TRANSAKSI
		}
		redirect('mst/area', 'refresh');
	} 	
	
	function edit($id) {
		$this->load->helper('text');
		$data['data']		= $this->_getData($id);
		$data['urlsegment']	= $this->uri->uri_string();
		$this->load->view('master/area/area_edit', $data);
	}
	
	function _editproc($id) {
		$submit			= $this->input->post('submit');
		$namaarea		= $this->input->post('areaname');
		$alamat			= $this->input->post('address');
		$kontak			= $this->input->post('contactperson');
		$userid			= $this->session->userdata('UserID');	
		
		if($submit=='SIMPAN') {			
			$this->db->trans_start();//-----------------------------------------------------START TRANSAKSI
			
			$data	= array(
							'AreaName'		=> $namaarea,
							'Address'		=> $alamat,
							'ContactPerson'	=> $kontak,
							'UserID'		=> $userid
					);
			$this->db->update('areamst', $data, array('AreaID'	=> $id));

			$this->db->trans_complete();//----------------------------------------------------END TRANSAKSI
		}
		
		// back to page asal	
		$urlstring	= $this->input->post('urlsegment');
		$urlarr = explode("/", $urlstring);
		$url	= '';
		$i	= 0;
		foreach ($urlarr as $uri) {
			if($i>5)
				$url	.= '/'.$uri;
			$i++;
		}
		redirect('mst/area'.$url, 'refresh');
	}	
	
	function _getLastInsertedID() {
		$sql = "SELECT LAST_INSERT_ID() AS ID";
		$query = $this->db->query($sql); 
		$result = $query->result_array();
		return $result[0]['ID'];
	}
	
	function test() {		
		print_array();
	}
} 