<html>
<head>
<title>BUKA HARI</title>
<link type="text/css" href="<?=base_url()?>publicfolder/cssdir/csstable/tablegrid.css" media="screen" rel="stylesheet" />
<?php
	$this->load->view('js/jqueryui');
?>
<script type="text/javascript">
$(function() {		
	
	$("#tglbuka").datepicker({	
<?php
	list($wbyr, $wbmth, $wbday) = explode('-',$wbdate); 
?>
		maxDate: new Date(<?=$wbyr?>, <?=$wbmth-1?>, <?=$wbday-0?>)
	});
});


</script>

<style>
	.fixwidthkecil { width:80px; }
	.fixwidthsedang { width:180px; }
	.ratakanan { text-align:right; }
	.fontkecil { font-size:60%; vertical-align:top;font-style:italic; }
	td { white-space: nowrap; }
</style>

</head>
<body>
<?php 
	menulist();
?>
<form action="<?=site_url()?>/trd/bukahari/bukaproc" method='post' id='formin'>
<br />
<br />
<br />
<table class='gridtable' width='340' align='center'>
	<thead>
		<tr><th colspan='2'>BUKA HARI</th></tr>
	</thead>
	<tr>
		<td align="center">
			Tgl Timbang
		</td>
		<td align="center">
			<input type='text' name='tglbuka' size='8' id='tglbuka' value="<?=$wbdate?>" readonly />
		</td>
	</tr>
	<tr>
		<td align="center" colspan='2'>
			<input type='submit' name='submit' value='PROSES' />
		</td>
	</tr>
</table>
</form>