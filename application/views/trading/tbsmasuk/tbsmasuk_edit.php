<html>
<head>
<title>TBS MASUK (edit)</title>
<link type="text/css" href="<?=base_url()?>publicfolder/cssdir/csstable/tablegrid.css" media="screen" rel="stylesheet" />
<?php
	$this->load->view('js/jqueryui');
	$this->load->view('js/SelectValidation');
	$this->load->view('js/TextValidation');
	$this->load->view('js/autocomplete');
	
	$datatbs	= $datatbs_arr[0];
	$datadtltbs	= $datadtltbs_arr[0];
?>
<script type="text/javascript">
$(function() {		
	$('#noregkendaraan').keyup(function() {
		hurufbesar(this);
	});
	
	$('#nospb').keyup(function() {
		hurufbesar(this);
	});
		
	startAutoSuggest();
	
	$('#noregkendaraan').focus(function() {
		clearautosuggest();
	});
	
	$('.inputanberat').focus(function() {
		clearthisinput(this);
	});
	
	$('.inputanberat').blur(function() {
		zeroifblank(this);
	});
	
	$("#tgltiket").datepicker({	
<?php
	list($wbyr, $wbmth, $wbday) = explode('-',$datatbs['DocketDate']); 
?>
		minDate: new Date(<?=$wbyr?>, <?=$wbmth-1?>, <?=$wbday-0?>)
	});

	$("#tglspb").datepicker();
		
	$('#beratkg').keyup(function() {
		hitungberat();
	});
	
	$('#tarra').keyup(function() {
		hitungberat();
	});
	
	$('#sortasiptg').keyup(function() {
		hitungberat();
	});
});

function startAutoSuggest() {	
		$("#noregkendaraan").coolautosuggest({
			url:'<?=site_url()?>/trd/tbsmasuk/getDataTruck_ajx/',
			minChars: 3,
			onSelected:function(result){
				if(result!=null){
					$("#namadriver").val(result.DriverName);
					$("#angkutan").val(result.ContractorID);
					hitungberat();
					$("#nospb").focus();
				}
				else{
					clearautosuggest();
				}
			}			
		});
	}
	
function clearautosuggest() {
	$("#noregkendaraan").val("");
	$("#namadriver").val("");
	$("#angkutan").val("");
	hitungberat();
}

function clearthisinput(obj) {
	$(obj).val("");
}

function zeroifblank(obj) {
	var thisval	= $(obj).val();
	if(thisval=='') {
		$(obj).val('0');
	}
	hitungberat();
}

function hitungberat() {
	var brutto			= $("#beratkg").val();
	var tarra			= $("#tarra").val();
	var sortasiptg		= $("#sortasiptg").val();
	
	var nettokg			= Number(brutto) - Number(tarra);
	var sortasikg		= nettokg * (sortasiptg/100);
	sortasikg			= Math.round(sortasikg / 10) * 10;
	var nettobayarkg	= nettokg - sortasikg;
	
	$("#sortasikg").val(sortasikg);
	$("#nettokg").val(nettokg);
	$("#nettobayarkg").val(nettobayarkg);	
}
	
function numformat(nStr) {
	nStr = nStr.replace( /\,/g, "");
	var x = nStr.split( '.' );
	var x1 = x[0];
	var x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while ( rgx.test(x1) ) {
		x1 = x1.replace( rgx, '$1' + '.' + '$2' );
	}
	return x1 + x2;
}

function hurufbesar(obj) {
	var strval	= $(obj).val();
	strval		= strval.toUpperCase();
	strval		= strval.replace(/\s/g,"");
	$(obj).val(strval);
}

function updateBlok(thisobj) {
	var areaid = $(thisobj).val();
	if(areaid != '') {
		$.post('<?=site_url()?>/trd/tbsmasuk/getBlok_ajx/'+areaid, function(data){
			$('#areablok').empty();
			$('#areablok').append(data);
		});
	}
}

</script>

<style>
	.msg {
		color: red;
		text-align: center;
		font-weight: bold;
	}
	.fixwidthkecil { width:80px; }
	.fixwidthsedang { width:180px; }
	.ratakanan { text-align:right; }
	.fontkecil { font-size:60%; vertical-align:top;font-style:italic; }
	td { white-space: nowrap; }
</style>

</head>
<body>
<?php 
	menulist();
?>
<form action="<?=site_url()?>/trd/tbsmasuk/editproc/<?=$datatbs['DocketID']?>" method='post' id='formin'>
<input type='hidden' name='urlsegment' id='urlsegment' value='<?=$urlsegment?>' />
<br />
<br />
<br />
<table width='600' align='center'>
	<tr>
		<td>
			<table class='gridtable' width='600'>
				<thead>
					<tr><th colspan='4'>TBS MASUK</th></tr>
				</thead>	
				<tr>
					<td align="right">
						No.Kendaraan
					</td>
					<td>
						<input type='text' name='noregkendaraan' size='10' id='noregkendaraan' value="<?=$datatbs['TruckID']?>" />
					</td>
					<td align="right">
						Tanggal Tiket
					</td>
					<td>
						<input type='text' name='tgltiket' size='8' id='tgltiket'  value="<?=$datatbs['DocketDate']?>" readonly />
					</td>
				</tr>
				<tr>
					<td align="right">
						Driver
					</td>
					<td>
						<input type='text' name='namadriver' size='18' id='namadriver' value="<?=$datatbs['DriverName']?>" />
					</td>
					<td align="right">
						Angkutan
					</td>
					<td>
						<?=form_dropdownDB_init('angkutan', $contractormst, 'ContractorID', 'ContractorName', $datatbs['ContractorID'], '', '--Pilih Angkutan--', 'id="angkutan" class="fixwidthsedang"')?>
					</td>
				</tr>
				<tr>
					<td align="right">
						No.SPB
					</td>	
					<td>
						<input type='text' name='nospb' size='18' id='nospb' value="<?=$datatbs['SPBID']?>" />
					</td>
					<td align="right">
						Tanggal SPB
					</td>
					<td>
						<input type='text' name='tglspb' size='8' id='tglspb' value="<?=$datatbs['SPBDate']?>" readonly />
					</td>
				</tr>
				<tr>
					<td align="right">
						Jam Masuk
					</td>
					<td>
						<input type='text' name='timein' size='6' id='timein' value="<?=substr($datatbs['TimeIn'],0,5)?>" />
					</td>
					<td align="right">
						Brutto
					</td>
					<td>
						<input type='text' name='beratkg' id='beratkg' class='ratakanan inputanberat' size='6' value="<?=$datatbs['GrossWgt']?>" /> Kg
					</td>
				</tr>
				<tr>
					<td align="right">
						Harga ( Rp )
					</td>
					<td>
						<input type='text' name='harga' size='6' id='harga' class='ratakanan inputanberat' value="<?=format_satu($datatbs['PriceAmt'])?>" />
					</td>
					<td align="right">
						Tarra
					</td>
					<td>
						<input type='text' name='tarra' id='tarra' value="<?=$datatbs['TareWgt']?>" class='ratakanan inputanberat' size='6' /> Kg
					</td>
				</tr>
				<tr>
					<td align="right">
						Sortasi ( % )
					</td>
					<td>
						<input type='text' name='sortasiptg' id='sortasiptg' class='ratakanan inputanberat' size='6' value="<?=$datatbs['SortasiPtg']?>" /> %
					</td>
					<td align="right">
						Sortasi ( Kg )
					</td>
					<td>
						<input type='text' name='sortasikg' id='sortasikg' value="<?=$datatbs['SortasiWgt']?>" class='ratakanan' size='6' readonly /> Kg
					</td>
				</tr>
				<tr>
					<td align="right">
						Netto
					</td>
					<td>
						<input type='text' name='nettokg' id='nettokg' value="<?=$datatbs['netto']?>" class='ratakanan' size='6' readonly /> Kg
					</td>
					<td align="right">
						Netto Bayar
					</td>
					<td>
						<input type='text' name='nettobayarkg' id='nettobayarkg' value="<?=$datatbs['nettobayar']?>" class='ratakanan' size='6' readonly /> Kg
					</td>
				</tr>
				<tr>
					<td align="right">
						Anggota KUD
					</td>
					<td>
						<?=form_dropdownDB_init('kudmemberid', $kudmembermst, 'KUDMemberID', 'KUDMemberName', $datatbs['KUDMemberID'], '', '--Pilih Anggota--', 'id="kudmemberid" class="fixwidthsedang"')?>
					</td>
					<td colspan="2">
						&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table class='gridtable' width='600'>
				<thead>
					<tr><th>Area</th><th>Blok</th><th>Jumlah Tandan</th></tr>
				</thead>
				<tr id="tr1">
					<td align="center">
						<?=form_dropdownDB_init('area', $areamst, 'AreaID', 'AreaName', $datadtltbs['AreaID'], '', '--Pilih Area--', 'id="areaname" onchange="updateBlok(this)" class="fixwidthsedang"')?>
					</td>
					<td align="center">
						<select name='areablok' id='areablok' class='fixwidthkecil'>
							<option value="<?=$datadtltbs['BlockID']?>"><?=$datadtltbs['BlockID']?></option>
						</select>
					</td>
					<td align="center">
						<input type='text' name='jumltandan' size='8' id='jumltandan' value="<?=$datadtltbs['FFBCnt']?>" class="ratakanan" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center">
			<input type='submit' name='submit' value='BATAL' />
			<input type='submit' name='submit' value='SIMPAN' />
		</td>
	</tr>
</table>

	
<p id="msg" class='msg'></p>
</form>
	<script>
		new Spry.Widget.ValidationSelect("areaname");
		new Spry.Widget.ValidationSelect("areablok");
		new Spry.Widget.ValidationSelect("angkutan");
		new Spry.Widget.ValidationTextField("jumltandan", "integer", { useCharacterMasking:true, minValue:0} );
		new Spry.Widget.ValidationTextField("noregkendaraan", "none");
		new Spry.Widget.ValidationTextField("nospb", "none");
		new Spry.Widget.ValidationTextField("namadriver", "none");
		new Spry.Widget.ValidationTextField("harga", "real", { useCharacterMasking:true, minValue:100.0} );
		new Spry.Widget.ValidationTextField("timein", "time", {format:"HH:mm", useCharacterMasking:true});
		
		new Spry.Widget.ValidationTextField("beratkg", "integer", { useCharacterMasking:true, minValue:0} );
		new Spry.Widget.ValidationTextField("tarra", "integer", { useCharacterMasking:true, minValue:0} );
		new Spry.Widget.ValidationTextField("sortasiptg", "real", { useCharacterMasking:true, minValue:0.0} );
		new Spry.Widget.ValidationTextField("sortasikg", "integer", { useCharacterMasking:true, minValue:0} );
		new Spry.Widget.ValidationTextField("nettokg", "integer", { useCharacterMasking:true, minValue:0} );
		new Spry.Widget.ValidationTextField("nettobayarkg", "integer", { useCharacterMasking:true, minValue:0} );
		
	</script>
</body>
</html>