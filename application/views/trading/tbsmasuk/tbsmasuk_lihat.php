<html>
<head>
<title>TBS MASUK (edit)</title>
<link type="text/css" href="<?=base_url()?>publicfolder/cssdir/csstable/tablegrid.css" media="screen" rel="stylesheet" />
<?php
	$this->load->view('js/jqueryui');
?>
<script type="text/javascript">


</script>

<style>
	.msg {
		color: red;
		text-align: center;
		font-weight: bold;
	}
	.fixwidthkecil { width:80px; }
	.fixwidthsedang { width:180px; }
	.ratakanan { text-align:right; }
	.fontkecil { font-size:60%; vertical-align:top;font-style:italic; }
	td { white-space: nowrap; }
</style>

</head>
<body>
<?php 
	menulist();
	$datatbs	= $datatbs_arr[0];
	$datadtltbs	= $datadtltbs_arr[0];
?>
<form action="<?=site_url()?>/trd/tbsmasuk/editproc/<?=$datatbs['DocketID']?>" method='post' id='formin'>
<input type='hidden' name='urlsegment' id='urlsegment' value='<?=$urlsegment?>' />
<br />
<br />
<br />
<table width='600' align='center'>
	<tr>
		<td>
			<table class='gridtable' width='600'>
				<thead>
					<tr><th colspan='4'>TBS MASUK</th></tr>
				</thead>	
				<tr>
					<td align="right">
						No.Kendaraan
					</td>
					<td>
						<input type='text' name='noregkendaraan' size='10' id='noregkendaraan' value="<?=$datatbs['TruckID']?>" readonly />
					</td>
					<td align="right">
						Tanggal Tiket
					</td>
					<td>
						<input type='text' name='tgltiket' size='8' id='tgltiket'  value="<?=$datatbs['DocketDate']?>" readonly />
					</td>
				</tr>
				<tr>
					<td align="right">
						Driver
					</td>
					<td>
						<input type='text' name='namadriver' size='18' id='namadriver' value="<?=$datatbs['DriverName']?>" readonly />
					</td>
					<td align="right">
						Angkutan
					</td>
					<td>
						<?=form_dropdownDB_init('angkutan', $contractormst, 'ContractorID', 'ContractorName', $datatbs['ContractorID'], '', '--Pilih Angkutan--', 'id="angkutan" class="fixwidthsedang" disabled')?>
					</td>
				</tr>
				<tr>
					<td align="right">
						No.SPB
					</td>	
					<td>
						<input type='text' name='nospb' size='18' id='nospb' value="<?=$datatbs['SPBID']?>" readonly />
					</td>
					<td align="right">
						Tanggal SPB
					</td>
					<td>
						<input type='text' name='tglspb' size='8' id='tglspb' value="<?=$datatbs['SPBDate']?>" readonly />
					</td>
				</tr>
				<tr>
					<td align="right">
						Jam Masuk
					</td>
					<td>
						<input type='text' name='timein' size='6' id='timein' value="<?=substr($datatbs['TimeIn'],0,5)?>" readonly />
					</td>
					<td align="right">
						Brutto
					</td>
					<td>
						<input type='text' name='beratkg' id='beratkg' class='ratakanan inputanberat' size='6' value="<?=$datatbs['GrossWgt']?>" readonly /> Kg
					</td>
				</tr>
				<tr>
					<td align="right">
						Harga ( Rp )
					</td>
					<td>
						<input type='text' name='harga' size='6' id='harga' class='ratakanan inputanberat' value="<?=format_satu($datatbs['PriceAmt'])?>" readonly />
					</td>
					<td align="right">
						Tarra
					</td>
					<td>
						<input type='text' name='tarra' id='tarra' value="<?=$datatbs['TareWgt']?>" class='ratakanan' size='6' readonly /> Kg
					</td>
				</tr>
				<tr>
					<td align="right">
						Sortasi ( % )
					</td>
					<td>
						<input type='text' name='sortasiptg' id='sortasiptg' class='ratakanan inputanberat' size='6' value="<?=$datatbs['SortasiPtg']?>" readonly /> %
					</td>
					<td align="right">
						Sortasi ( Kg )
					</td>
					<td>
						<input type='text' name='sortasikg' id='sortasikg' value="<?=$datatbs['SortasiWgt']?>" class='ratakanan' size='6' readonly /> Kg
					</td>
				</tr>
				<tr>
					<td align="right">
						Netto
					</td>
					<td>
						<input type='text' name='nettokg' id='nettokg' value="<?=$datatbs['netto']?>" class='ratakanan' size='6' readonly /> Kg
					</td>
					<td align="right">
						Netto Bayar
					</td>
					<td>
						<input type='text' name='nettobayarkg' id='nettobayarkg' value="<?=$datatbs['nettobayar']?>" class='ratakanan' size='6' readonly /> Kg
					</td>
				</tr>
				<tr>
					<td align="right">
						Anggota KUD
					</td>
					<td>
						<?=form_dropdownDB_init('kudmemberid', $kudmembermst, 'KUDMemberID', 'KUDMemberName', $datatbs['KUDMemberID'], '', '--Pilih Anggota--', 'id="kudmemberid" class="fixwidthsedang" disabled')?>
					</td>
					<td colspan="2">
						&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table class='gridtable' width='600'>
				<thead>
					<tr><th>Area</th><th>Blok</th><th>Jumlah Tandan</th></tr>
				</thead>
				<tr id="tr1">
					<td align="center">
						<?=form_dropdownDB_init('area', $areamst, 'AreaID', 'AreaName', $datadtltbs['AreaID'], '', '--Pilih Area--', 'id="areaname" onchange="updateBlok(this)" class="fixwidthsedang" disabled')?>
					</td>
					<td align="center">
						<select name='areablok' id='areablok' class='fixwidthkecil' disabled>
							<option value="<?=$datadtltbs['BlockID']?>"><?=$datadtltbs['BlockID']?></option>
						</select>
					</td>
					<td align="center">
						<input type='text' name='jumltandan' size='8' id='jumltandan' value="<?=$datadtltbs['FFBCnt']?>" class="ratakanan" readonly />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center">
			<input type='submit' name='submit' value='KEMBALI' />
		</td>
	</tr>
</table>

	
<p id="msg" class='msg'></p>
</form>
</body>
</html>