<!DOCTYPE html>
<html>
<head>
	<title>TBS Masuk</title>
<link type="text/css" href="<?=base_url()?>publicfolder/cssdir/csstable/tablegrid.css" media="screen" rel="stylesheet" />
<link type="text/css" href="<?=base_url()?>publicfolder/cssdir/csstable/tablegrid2.css" media="screen" rel="stylesheet" />
<link type="text/css" href="<?=base_url()?>publicfolder/cssdir/csspaging/paging.css" media="screen" rel="stylesheet" />
<?php $this->load->view('js/jqueryui')?>
<style>
.ui-dialog-title, .ui-dialog-titlebar, .ui-dialog-titlebar-close{
	font-size:small;
}
.ui-icon {
	 cursor: pointer; cursor: hand;
}
.ratakanan{
	text-align:right;
}
.rtengah{
	text-align:center;
}
.lebariconkecil{
	width:17px;
}
.lebariconsedang{
	width:35px;
}
.lebariconbesar{
	width:68px;
}
.lebardropdown{
	width:200px;
}
</style>
<script language="javascript">
	
function validateEdit(DocketID, url) {
	window.location='<?=site_url()?>/trd/tbsmasuk/edit/'+DocketID+url
}

function validateCetak(DocketID) {
	window.location='<?=site_url()?>/trd/tbsmasuk/cetak/'+DocketID
}

</script>
</head>
<body>
<?php menulist()?>
<form action=<?=site_url().'/trd/tbsmasuk/'?> method='post'>
<table align=center border="0" cellpadding="0" cellspacing="3" width="350" class='gridtable'>
	<thead>
	  <tr><th colspan='2'>TBS MASUK</th></tr>
  	</thead>
      <tr>
        <td noWrap align=center>
		<select name="option">
			<option value='f.DocketID'>No Tiket</option>
			<option value='a.AreaName'>Area / KUD</option>
			<option value='f.DocketDate'>Tanggal</option>
		</select>
		&nbsp;&nbsp;<input type=text size='10' name='optionValue' />&nbsp;&nbsp;<input type=submit value='Cari' name='submit' />
		</td>
	  </tr>
</table>

<br><br>

<table align=center border="0" cellpadding="0" cellspacing="3" width="640" class='gridua'>
	<thead>
	  <tr>
		<th>Tanggal</th>
	  	<th>No. Tiket</th>
	  	<th>No. Kendaraan</th>
	  	<th>Area/KUD</th>
		<th>Netto (Kg)</th>
	  	<th colspan='6'>
			<table align="center">
				<tr>
					<td>
						<a href="<?=site_url()?>/trd/tbsmasuk/input">
						<div class='ui-state-default ui-corner-all lebariconkecil' title='INPUT TBS MASUK'>
							<span class='ui-icon ui-icon-plusthick' />
						</div
					</a>
					</td>
				</tr>
			</table>
		</th>
	  </tr>
	</thead>
	<tbody>
<?php 
for($a=0; $a<count($view_data); $a++) {
	
	$notiket		= $view_data[$a]['DocketID'];
	$tgltiket		= $view_data[$a]['DocketDate'];
	$nokendaraan	= $view_data[$a]['TruckID'];
	$areakud		= $view_data[$a]['AreaName'];
	$berat			= $view_data[$a]['FFBWeight'];
	$status			= ($wbdate>$tgltiket)?1:0;
?>
	  <tr>
	  	<td align='center'><?=$tgltiket?></td>
	  	<td align='center'><?=$notiket?></td>
	  	<td align='left'><?=$nokendaraan?></td>
		<td align='left'><?=$areakud?></td>
		<td align='right'><?=format_tiga($berat)?></td>	  	
		<td width=20 align='center'>
			<?php 
				$url	='/'.$this->uri->uri_string();
				if($status<1) {					
			?>
			<div class='ui-state-default ui-corner-all lebariconkecil' title='EDIT' onclick="validateEdit('<?=$notiket?>','<?=$url?>')">
				<span class='ui-icon ui-icon-pencil' />
			</div>
				<?php } else { ?>
			&nbsp;
				<?php } ?>
		</td>
	  	<td width=20 align='center'>
		  <a href="<?=site_url()?>/trd/tbsmasuk/lihat/<?=$notiket.$url?>">
			<div class='ui-state-default ui-corner-all lebariconkecil' title='LIHAT'>
				<span class='ui-icon ui-icon-search' />
			</div>
		  </a>
		</td>
		<td width=20 align='center'>
			<div class='ui-state-default ui-corner-all lebariconkecil' onclick="validateCetak('<?=$notiket;?>')"  title='CETAK TIKET'>
				<span class='ui-icon ui-icon-document-b' />
			</div>
		</td>
	  </tr>
<?php } ?>
	</tbody>
</table>
<br><br>
<?=$this->pagination->create_links()?>
<br><br>
</form>
</body>
</html>