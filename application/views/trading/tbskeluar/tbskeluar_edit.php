<html>
<head>
<title>TBS KELUAR (EDIT)</title>
<link type="text/css" href="<?=base_url()?>publicfolder/cssdir/csstable/tablegrid.css" media="screen" rel="stylesheet" />
<?php
	$this->load->view('js/jqueryui');
	$this->load->view('js/SelectValidation');
	$this->load->view('js/TextValidation');
	$this->load->view('js/autocomplete');
	$datatbs	= $datatbs_arr[0];
?>
<script type="text/javascript">
$(function() {		
	$('#noregkendaraan').keyup(function() {
		hurufbesar(this);
	});
	
	$('#nospb').keyup(function() {
		hurufbesar(this);
	});
	
	startAutoSuggest();
	
	$('#noregkendaraan').focus(function() {
		clearautosuggest();
	});
	
	$('.inputanberat').focus(function() {
		clearthisinput(this);
	});
	
	$('.inputanberat').blur(function() {
		zeroifblank(this);
	});
	
	$("#tgltiket").datepicker({	
<?php
	list($wbyr, $wbmth, $wbday) = explode('-',$datatbs['DocketDate']); 
?>
		minDate: new Date(<?=$wbyr?>, <?=$wbmth-1?>, <?=$wbday-0?>),
		onSelect: function(date) {
            getTotalAnggotaByYear();
        }
	});

	$("#tglspb").datepicker();
	
	$('#kudmemberid').change(function() {
		getTotalAnggotaByYear();
	});
	
	$('#harga').keyup(function() {
		hitungtotalmaxanggota();
	});
	
	$('#beratkg').keyup(function() {
		hitungberat();
		hitungtotalmaxanggota();
		hitungstok();
	});
	
	$('#tarra').keyup(function() {
		hitungberat();
		hitungtotalmaxanggota();
		hitungstok();
	});
	
	$('#nettoditerimakg').keyup(function() {
		hitungtotalmaxanggota();
	});
	
	getTotalAnggotaByYear();
});

function getTotalAnggotaByYear() {
	var tgltiket = $('#tgltiket').val();
	var anggota	= $('#kudmemberid').val();
	if(anggota !=='') {
		$.post("<?=site_url()?>/trd/tbskeluar/getTotalAnggotaByYear_ajx", { tgltiket: tgltiket, anggota: anggota }, function(data) { 
			jsdata = data;
			eval(jsdata);
			$('#totalyearbyanggota').val(datajson.TotalVal);
			$('#totalstokbyanggota').val(datajson.TotalStok);
			hitungtotalmaxanggota();
			hitungstok();
	   });
	} else {
		$('#totalyearbyanggota').val("0");
		$('#totalstokbyanggota').val("0");
		hitungtotalmaxanggota();
		hitungstok();
	}
}

function hitungstok() {
	var totalstok = Number($('#totalstokbyanggota').val());
	var netto = Number($('#nettokg').val());
	var sisa	= totalstok - netto;
//	var sisastr	= numformat(String(sisa));
	$('#sisastok').html(sisa);
	
}

function hitungtotalmaxanggota() {
	var totalmax	= 4500000000;
	var totalval = Number($('#totalyearbyanggota').val());
	var harga = Number($('#harga').val());
	var netto = Number($('#nettoditerimakg').val());
	var curval = netto * harga;
	var totalcurval = curval + totalval;
	
	if(totalcurval > totalmax) {
		alert('Melewati Nilai Max Per Anggota dalam setahun !' + "\n"+numformat(String(totalcurval)));
		$('#harga').val("0");
	}
}

function startAutoSuggest() {	
		$("#noregkendaraan").coolautosuggest({
			url:'<?=site_url()?>/trd/tbskeluar/getDataTruck_ajx/',
			minChars: 3,
			onSelected:function(result){
				if(result!=null){
					$("#namadriver").val(result.DriverName);
					$("#angkutan").val(result.ContractorID);
					hitungberat();
					$("#nospb").focus();
				}
				else{
					clearautosuggest();
				}
			}			
		});
	}
	
function clearautosuggest() {
	$("#noregkendaraan").val("");
	$("#namadriver").val("");
	$("#angkutan").val("");
	hitungberat();
}

function clearthisinput(obj) {
	$(obj).val("");
}

function zeroifblank(obj) {
	var thisval	= $(obj).val();
	if(thisval=='') {
		$(obj).val('0');
	}
	hitungberat();
}

function hitungberat() {
	var brutto			= $("#beratkg").val();
	var tarra			= $("#tarra").val();
	
	var nettokg			= Number(brutto) - Number(tarra);
	
	$("#nettokg").val(nettokg);
	$("#nettoditerimakg").val(nettokg);
}
	
function numformat(nStr) {
	nStr = nStr.replace( /\,/g, "");
	var x = nStr.split( '.' );
	var x1 = x[0];
	var x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while ( rgx.test(x1) ) {
		x1 = x1.replace( rgx, '$1' + '.' + '$2' );
	}
	return x1 + x2;
}

function hurufbesar(obj) {
	var strval	= $(obj).val();
	strval		= strval.toUpperCase();
	strval		= strval.replace(/\s/g,"");
	$(obj).val(strval);
}

</script>

<style>
	.msg {
		color: red;
		text-align: center;
		font-weight: bold;
	}
	.fixwidthkecil { width:80px; }
	.fixwidthsedang { width:180px; }
	.ratakanan { text-align:right; }
	.fontkecil { font-size:60%; vertical-align:top;font-style:italic; }
	td { white-space: nowrap; }
</style>

</head>
<body>
<?php 
	menulist();	
?>
<form action="<?=site_url()?>/trd/tbskeluar/editproc/<?=$datatbs['DocketID']?>" method='post' id='formin'>
<input type='hidden' name='urlsegment' id='urlsegment' value='<?=$urlsegment?>' />
<br />
<br />
<br />
<input type='hidden' name='totalstokbyanggota' id='totalstokbyanggota' value="0" />
<input type='hidden' name='totalyearbyanggota' id='totalyearbyanggota' value="0" />
<table width='600' align='center'>
	<tr>
		<td>
			<table class='gridtable' width='600'>
				<thead>
					<tr><th colspan='4'>TBS KELUAR</th></tr>
				</thead>	
				<tr>
					<td align="right">
						No.Kendaraan
					</td>
					<td>
						<input type='text' name='noregkendaraan' size='10' id='noregkendaraan' value="<?=$datatbs['TruckID']?>" />
					</td>
					<td align="right">
						Tanggal Tiket
					</td>
					<td>
						<input type='text' name='tgltiket' size='8' id='tgltiket' value="<?=$datatbs['DocketDate']?>" readonly />
					</td>
				</tr>
				<tr>
					<td align="right">
						Driver
					</td>
					<td>
						<input type='text' name='namadriver' size='18' id='namadriver' value="<?=$datatbs['DriverName']?>" />
					</td>
					<td align="right">
						Angkutan
					</td>
					<td>
						<?=form_dropdownDB_init('angkutan', $contractormst, 'ContractorID', 'ContractorName', $datatbs['ContractorID'], '', '--Pilih Angkutan--', 'id="angkutan" class="fixwidthsedang"')?>
					</td>
				</tr>
				<tr>
					<td>
						Customer
					</td>
					<td>
						<?=form_dropdownDB_init('customerid', $customermst, 'CustomerID', 'CustomerName', $datatbs['CustomerID'], '', '--Pilih Customer--', 'id="customerid" class="fixwidthsedang"')?>
					</td>
					<td align="right">
						Tanggal SPB
					</td>
					<td>
						<input type='text' name='tglspb' size='8' id='tglspb' value="<?=$datatbs['SPBDate']?>" readonly />
					</td>
				</tr>
				<tr>
					<td>
						Anggota KUD
					</td>
					<td>
						<?=form_dropdownDB_init('kudmemberid', $kudmembermst, 'KUDMemberID', 'KUDMemberName', $datatbs['KUDMemberID'], '', '--Pilih Anggota--', 'id="kudmemberid" class="fixwidthsedang"')?>
					</td>
					<td align="right">
						No.SPB
					</td>	
					<td>
						<input type='text' name='nospb' size='18' id='nospb' value="<?=$datatbs['SPBID']?>" />
					</td>
				</tr>
				<tr>
					<td align="right">
						Jam Keluar
					</td>
					<td>
						<input type='text' name='timeout' size='6' id='timeout' value="<?=substr($datatbs['TimeOut'],0,5)?>" />
					</td>
					<td align="right">
						Brutto
					</td>
					<td>
						<input type='text' name='beratkg' id='beratkg' value="<?=format_satu($datatbs['GrossWgt'])?>" class='ratakanan inputanberat' size='6' /> Kg
					</td>
				</tr>
				<tr>
					<td align="right">
						Harga ( Rp )
					</td>
					<td>
						<input type='text' name='harga' size='6' id='harga' class='ratakanan' value="<?=format_satu($datatbs['PriceAmt'])?>" />
					</td>
					<td align="right">
						Tarra
					</td>
					<td>
						<input type='text' name='tarra' id='tarra' value="<?=format_satu($datatbs['TareWgt'])?>" class='ratakanan inputanberat' size='6' /> Kg
					</td>
				</tr>
				<tr>
					<td align="right">
						Netto Diterima
					</td>
					<td>
						<input type='text' name='nettoditerimakg' id='nettoditerimakg' value="<?=format_satu($datatbs['ReceivedWgt'])?>" class='ratakanan' size='6' /> Kg						
					</td>
					<td align="right">
						Netto Kirim
					</td>
					<td>
						<input type='text' name='nettokg' id='nettokg' value="<?=format_satu($datatbs['netto'])?>" class='ratakanan' size='6' readonly /> Kg
					</td>
				</tr>
				<tr>
					<td colspan="4" align="right">
						Sisa Stok <span id="sisastok"></span> Kg
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center">
			<input type='submit' name='submit' value='BATAL' />
			<input type='submit' name='submit' value='SIMPAN' />
		</td>
	</tr>
</table>

	
<p id="msg" class='msg'></p>
</form>
	<script>
		new Spry.Widget.ValidationSelect("angkutan");
		new Spry.Widget.ValidationSelect("customerid");
		new Spry.Widget.ValidationSelect("kudmemberid");
		new Spry.Widget.ValidationTextField("noregkendaraan", "none");
		new Spry.Widget.ValidationTextField("nospb", "none");
		new Spry.Widget.ValidationTextField("namadriver", "none");
		new Spry.Widget.ValidationTextField("harga", "real", { useCharacterMasking:true, minValue:0.0} );
		new Spry.Widget.ValidationTextField("timeout", "time", {format:"HH:mm", useCharacterMasking:true});
		
		new Spry.Widget.ValidationTextField("beratkg", "integer", { useCharacterMasking:true, minValue:0} );
		new Spry.Widget.ValidationTextField("tarra", "integer", { useCharacterMasking:true, minValue:0} );
		new Spry.Widget.ValidationTextField("nettokg", "integer", { useCharacterMasking:true, minValue:0} );
		new Spry.Widget.ValidationTextField("nettoditerimakg", "integer", { useCharacterMasking:true, minValue:0} );
	</script>
</body>
</html>