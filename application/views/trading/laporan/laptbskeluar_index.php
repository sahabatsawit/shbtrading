<!DOCTYPE html>
<html>
<head>
	<title>LAPORAN TBS KELUAR</title>
<link type="text/css" href="<?=base_url()?>publicfolder/cssdir/csstable/tablegrid.css" media="screen" rel="stylesheet" />
<?php $this->load->view('js/jqueryui')?>
<style>
.ui-dialog-title, .ui-dialog-titlebar, .ui-dialog-titlebar-close{
	font-size:small;
}
.ui-icon {
	 cursor: pointer; cursor: hand;
}
.ratakanan{
	text-align:right;
}
.rtengah{
	text-align:center;
}
.lebariconkecil{
	width:17px;
}
.lebariconsedang{
	width:35px;
}
.lebariconbesar{
	width:68px;
}
.lebardropdown{
	width:200px;
}
</style>

<script>
$(document).ready(function() {
	$("#from").datepicker({
  changeMonth: true,
  changeYear: true
});
	$("#until").datepicker({
  changeMonth: true,
  changeYear: true
});
});
</script>

</head>
<body>
<?php menulist()?>
<form action='<?=site_url()?>/trd/laptbskeluar/cetak' method='post'>
	<br />
	<br />
	<br />
<table align=center border="0" cellpadding="0" cellspacing="3" width="350" class='gridtable'>
	<thead>
		<tr>
			<th colspan="2">LAPORAN TBS KELUAR</th>
		</tr>
	</thead>
	<tr>
		<td>
			Tanggal &nbsp;&nbsp;
		</td>
		<td>
			<input type="text" name="from" id="from" size="8" readonly>
				s/d 
			<input type="text" name="until" id="until" size="8" readonly >
		</td>
	</tr>
	<tr>
		<td>
			Pelanggan &nbsp;&nbsp;
		</td>
		<td>
			<select name='CustomerID' class="lebardropdown">
				<option value='all'>Semua</option>
				<?php for($a=0 ; $a<count($customermst) ; $a++) { ?>
				<option value='<?=$customermst[$a]['CustomerID'];?>'><?=$customermst[$a]['CustomerName'];?></option>
				<?php } ?>
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<input type='submit' value="PDF" name='submit' />
			<input type='submit' value=CSV name='submit' />
		</td>
	</tr>	
</table>
</form>
</body>
</html>
	