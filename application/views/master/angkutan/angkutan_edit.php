<html>
<head>
<title>Angkutan (input/edit)</title>
<link type="text/css" href="<?=base_url()?>publicfolder/cssdir/csstable/tablegrid.css" media="screen" rel="stylesheet" />
<?php
	$this->load->view('js/jqueryui');
	$this->load->view('js/TextValidation');
?>
<style>
	.msg {
		color: red;
		text-align: center;
		font-weight: bold;
	}
	.fixwidthkecil { width:80px; }
	.fixwidthsedang { width:180px; }
	.ratakanan { text-align:right; }
	.fontkecil { font-size:60%; vertical-align:top;font-style:italic; }
	td { white-space: nowrap; }
</style>

</head>
<body>
<?php 
	menulist();
?>
<form action="<?=site_url()?>/mst/angkutan/inputeditproc/<?=$data['ContractorID']?>" method='post' id='formin'>
<input type='hidden' name='urlsegment' id='urlsegment' value='<?=$urlsegment?>' />
<br />
<br />
<br />
<table width='600' align='center'>
	<tr>
		<td>
			<table class='gridtable' width='600'>
				<thead>
					<tr><th colspan='4'>ANGKUTAN</th></tr>
				</thead>	
				<tr>
					<td align="right">
						Nama Angkutan
					</td>
					<td>
						<input type='text' name='contractorname' size='30' id='contractorname' value="<?=$data['ContractorName']?>" />
					</td>
				</tr>
				<tr>
					<td align="right">
						Alamat
					</td>
					<td>
						<textarea rows="4" cols="30" name='address'><?=$data['Address']?></textarea>
					</td>
				</tr>
				<tr>
					<td align="right">
						Telp
					</td>
					<td>
						<input type='text' name='telp' size='20' id='telp' value="<?=$data['Telp']?>" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center">
			<input type='submit' name='submit' value='BATAL' />
			<input type='submit' name='submit' value='SIMPAN' />
		</td>
	</tr>
</table>
</form>
	<script>
		new Spry.Widget.ValidationTextField("contractorname", "none");
		new Spry.Widget.ValidationTextField("telp", "none");
		new Spry.Widget.ValidationTextField("contactname", "none");
	</script>
</body>
</html>