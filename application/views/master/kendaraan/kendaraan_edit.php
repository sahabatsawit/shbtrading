<html>
<head>
<title>Kendaraan (input/edit)</title>
<link type="text/css" href="<?=base_url()?>publicfolder/cssdir/csstable/tablegrid.css" media="screen" rel="stylesheet" />
<?php
	$this->load->view('js/jqueryui');
	$this->load->view('js/SelectValidation');
	$this->load->view('js/TextValidation');
?>
<script type="text/javascript">
$(function() {	
	$('#truckid').keyup(function() {
		hurufbesar(this);
	});
});

function hurufbesar(obj) {
	var strval	= $(obj).val();
	strval		= strval.toUpperCase();
	strval		= strval.replace(/\s/g,"");
	$(obj).val(strval);
}
</script>

<style>
	.msg {
		color: red;
		text-align: center;
		font-weight: bold;
	}
	.fixwidthkecil { width:80px; }
	.fixwidthsedang { width:180px; }
	.ratakanan { text-align:right; }
	.fontkecil { font-size:60%; vertical-align:top;font-style:italic; }
	td { white-space: nowrap; }
</style>

</head>
<body>
<?php 
	menulist();
?>
<form action="<?=site_url()?>/mst/kendaraan/inputeditproc/<?=$data['TruckID']?>" method='post' id='formin'>
<input type='hidden' name='urlsegment' id='urlsegment' value='<?=$urlsegment?>' />
<br />
<br />
<br />
<table width='600' align='center'>
	<tr>
		<td>
			<table class='gridtable' width='600'>
				<thead>
					<tr><th colspan='4'>KENDARAAN</th></tr>
				</thead>	
				<tr>
					<td align="right">
						No Polisi
					</td>
					<td>
						<?php 
						$readonlystr	= is_null($data['TruckID'])?'':'readonly ';
						?>
						<input type='text' name='truckid' size='16' id='truckid' value="<?=$data['TruckID']?>" <?=$readonlystr?>/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Angkutan
					</td>
					<td>
						<?=form_dropdownDB_init('angkutan', $contractormst, 'ContractorID', 'ContractorName', $data['ContractorID'], '', '--Pilih Angkutan--', 'id="angkutan" class="fixwidthsedang"')?>
					</td>
				</tr>
				<tr>
					<td align="right">
						Driver
					</td>
					<td>
						<input type='text' name='drivername' size='20' id='drivername' value="<?=$data['DriverName']?>" />
					</td>
				</tr>
				<tr>
					<td align="right">
						Type Kendaraan
					</td>
					<td>
						<input type='text' name='trucktype' size='20' id='trucktype' value="<?=$data['TruckType']?>" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center">
			<input type='submit' name='submit' value='BATAL' />
			<input type='submit' name='submit' value='SIMPAN' />
		</td>
	</tr>
</table>
</form>
	<script>
		new Spry.Widget.ValidationTextField("truckid", "none");
		new Spry.Widget.ValidationTextField("drivername", "none");
		new Spry.Widget.ValidationTextField("trucktype", "none");
		new Spry.Widget.ValidationSelect("angkutan");
	</script>
</body>
</html>