<!DOCTYPE html>
<html>
<head>
	<title>Area</title>
<link type="text/css" href="<?=base_url()?>publicfolder/cssdir/csstable/tablegrid.css" media="screen" rel="stylesheet" />
<link type="text/css" href="<?=base_url()?>publicfolder/cssdir/csstable/tablegrid2.css" media="screen" rel="stylesheet" />
<link type="text/css" href="<?=base_url()?>publicfolder/cssdir/csspaging/paging.css" media="screen" rel="stylesheet" />
<?php $this->load->view('js/jqueryui')?>
<style>
.ui-dialog-title, .ui-dialog-titlebar, .ui-dialog-titlebar-close{
	font-size:small;
}
.ui-icon {
	 cursor: pointer; cursor: hand;
}
.ratakanan{
	text-align:right;
}
.rtengah{
	text-align:center;
}
.lebariconkecil{
	width:17px;
}
.lebariconsedang{
	width:35px;
}
.lebariconbesar{
	width:68px;
}
.lebardropdown{
	width:200px;
}
</style>
</head>
<body>
<?php menulist()?>
<form action=<?=site_url().'/mst/area/'?> method='post'>
<table align=center border="0" cellpadding="0" cellspacing="3" width="350" class='gridtable'>
	<thead>
	  <tr><th colspan='2'>AREA / KUD</th></tr>
  	</thead>
      <tr>
        <td noWrap align=center>
		<select name="option">
			<option value='AreaName'>Nama Area /KUD</option>
			<option value='ContactPerson'>Kontak</option>
			<option value='Address'>Alamat</option>
		</select>
		&nbsp;&nbsp;<input type=text size='10' name='optionValue' />&nbsp;&nbsp;<input type=submit value='Cari' name='submit' />
		</td>
	  </tr>
</table>

<br><br>

<table align=center border="0" cellpadding="0" cellspacing="3" width="800" class='gridua'>
	<thead>
	  <tr>
		<th>Nama Area / KUD</th>
	  	<th>Alamat</th>
	  	<th>Kontak</th>
	  	<th colspan='6'>
			<table align="center">
				<tr>
					<td>
						<a href="<?=site_url()?>/mst/area/input">
							<div class='ui-state-default ui-corner-all lebariconkecil' title='INPUT AREA/KUD BARU'>
								<span class='ui-icon ui-icon-plusthick' />
							</div
						</a>
					</td>
				</tr>
			</table>
		</th>
	  </tr>
	</thead>
	<tbody>
<?php 
for($a=0; $a<count($view_data); $a++) {
	
	$id				= $view_data[$a]['AreaID'];
	$AreaName		= $view_data[$a]['AreaName'];
	$Address		= $view_data[$a]['Address'];
	$Contact		= $view_data[$a]['ContactPerson'];
?>
	  <tr>
	  	<td align='left'><?=$AreaName?></td>
		<td align='left'><?=substr($Address,0,40)?></td>
	  	<td align='left'><?=$Contact?></td>
		<td width=20 align='center'>
			<?php 
				$url	=$this->uri->uri_string();
			?>
			<a href="<?=site_url()?>/mst/area/edit/<?=$id?>/<?=$url?>">
				<div class='ui-state-default ui-corner-all lebariconkecil' title='EDIT AREA/KUD'>
					<span class='ui-icon ui-icon-pencil' />
				</div>
			</a>
		</td>
	  </tr>
<?php } ?>
	</tbody>
</table>
<br><br>
<?=$this->pagination->create_links()?>
<br><br>
</form>
</body>
</html>